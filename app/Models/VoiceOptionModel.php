<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;

/**
 * Class ContentCategoryModel
 * @package App\Models
 *
 * @property int $car_number
 * @property string $note
 */
class VoiceOptionModel extends BaseModel
{
    protected $table = 'voice_option';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;
    protected $allowedFields = ['number_alert', 'alert_time_loop', 'type_alert', 'time_wait_come_in'];
    /**
     * @param string|null $scenario
     * @return array
     */
    public function getRules(string $scenario = null): array
    {
        return [
            'number_alert' =>[
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Số lần thông báo không được để trống',
                ]
            ],
            'alert_time_loop' =>[
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Thời gian lặp lại không được để trống',
                ]
            ],
            'type_alert' =>[
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Kiểu thông báo không được để trống',
                ]
            ],
        ];
    }


}