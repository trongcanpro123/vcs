<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;
use CodeIgniter\Model;

/**
 * Class ContentCategoryModel
 * @package App\Models
 *
 * @property string $title
 * @property string $slug
 * @property int $category_id
 * @property string $intro
 * @property string $content
 * @property string $image
 * @property string $material
 * @property string $guarantee
 * @property int $price
 * @property int $discount
 * @property int $is_lock
 */
class CarInOutHistoryModel extends BaseModel
{
    protected $table = 'car_in_out_history';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;

    protected $allowedFields = ['qr_value','smo_driver_name','smo_car_number','image_capture','driver_image_detected',
            'driver_name_detected','car_number_image_detected','car_number_detected','driver_name','car_number',
            'checkin_time','checkin_note','checkin_by','checkin_order','ticket','ticket_order','ticket_alert',
            'checkout_order_status','checkout_image','checkout_car_number_image_detected','checkout_car_number_detected',
            'checkout_car_number','checkout_reason','checkout_time','area_id', 'created_at','updated_at',
            'checkin_car_number_hand', 'checkin_driver_hand','checkin_car_number_note','checkin_driver_note','customer_type',
            'checkout_by', 'invite_take_item', 'note'
        ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
//    protected $deletedField = 'deleted_at';
    protected $dateFormat = 'int';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    public function getRules(string $scenario = null): array
    {
        return [
            ''
        ];
    }

    public function getRowNearDown($area_id, $ticket_order){
        $query_find_near = 'select * from  car_in_out_history where area_id = ? and ticket = 0 and ticket_order > ? and checkin_order > 0 order by ticket_order asc ';
        $row_down_near = $this->db->query($query_find_near,[$area_id, $ticket_order])->getRow();
        return $row_down_near ;
    }



    public function get_image_front_driver(){

        if($this->driver_name_detected){
            $driver = (new DriverModel())->where('id',$this->driver_name_detected)->first();
            if($driver){
                return  $driver->image_front;
            }
        }
        return '';
    }
    public function get_driver_name(){

        if($this->driver_name_detected){
            $driver = (new DriverModel())->where('id',$this->driver_name_detected)->first();
            if($driver){
                return  $driver->driver_name;
            }
        }
        return '';
    }

    public function is_exist_car_number(){
        if($this->car_number_detected){
            $car_model = (new CarModel())->where('car_number', $this->car_number_detected)->first();
            if($car_model){
                return true;
            }
        }

        return false;
    }

    public function set_order_number($id,$area_id){

        $this->db->query('update car_in_out_history set 
                                                        checkin_order = (select COALESCE(MAX(checkin_order),0) + 1  AS checkin_order 
                                                        from car_in_out_history where checkin_order>0 and area_id = ? and DATE(checkin_time) = DATE(NOW()) )
                            where id = ?' ,[$area_id, $id]
                            );
       $this->db->query('update car_in_out_history set ticket_order = checkin_order  where id = ?' ,[$id] );
       $data = $this->db->query('select checkin_order from car_in_out_history where id = ?',$id)->getRow();
       if($data){
           return $data->checkin_order;
       }
       return 0;
    }
    public function reset_order_get_ticket($id,$area_id){

        $this->db->query('update car_in_out_history set 
                                                        ticket_order = (select COALESCE(MAX(ticket_order),0) +1  AS ticket_order 
                                                        from car_in_out_history where ticket_order>0 and area_id = ? and DATE(checkin_time) = DATE(NOW()) ),
                                                        ticket_alert = 0
                            where id = ?' ,[$area_id, $id]
        );
        $data = $this->db->query('select ticket_order from car_in_out_history where id = ?',$id)->getRow();
        if($data){
            return $data->ticket_order;
        }
        return 0;
    }
    public function get_car_checkout_by_car_number($car_number, $area_id ){
        $data = $this->db->query('select id from car_in_out_history where car_number = ? and area_id = ? 
                                    and checkin_order > 0 order by id desc limit 1 ',[$car_number, $area_id])->getRow();
        if($data){
            return $data->id;
        }
        return 0;
    }



    public function get_list_in_out_in_alert_take_ticket($area_id, $minutes_alert){

        $data = $this->db->query('select * from car_in_out_history where area_id = ? and ticket =0 and ticket_alert = 1 
                                   and checkout_time IS NULL 
                                   and  TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(updated_at),NOW()) < ? ',[$area_id, $minutes_alert])->getResult() ;
        return $data;
    }

    public function get_trang_thai_lay_hang(){
        if($this->invite_take_item == 0){
            return 'Đang chờ';
        }
        if($this->invite_take_item and !$this->checkout_time){
            $exits_dang_xuat = (new TgbxOrderDetailModel())->where('in_out_id', $this->id)->where('tdh_status','dang_xuat')->first();
            if($exits_dang_xuat){
                return 'Đang bơm';
            }
            $exits_dang_cho = (new TgbxOrderDetailModel())->where('in_out_id', $this->id)->where('tdh_status','dang_cho')->first();
            $exits_ket_thuc = (new TgbxOrderDetailModel())->where('in_out_id', $this->id)->where('tdh_status','ket_thuc')->first();

            if($exits_dang_cho && $exits_ket_thuc){
                return  'Đang bơm';
            }

            if($exits_dang_cho && !$exits_ket_thuc){
                return  'Chuẩn bị';
            }

        }
        if($this->checkout_order_status == '1' ){
            return 'Hoàn thành';
        }
        return '';
    }
}
