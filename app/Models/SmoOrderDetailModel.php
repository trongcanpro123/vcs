<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;

/**
 * Class ContentCategoryModel
 * @package App\Models
 *
 * @property int $car_number
 * @property string $note
 */
class SmoOrderDetailModel extends BaseModel
{
    protected $table = 'smo_order_detail';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;
    protected $allowedFields = ['in_out_history_id', 'po_code', 'doc_date', 'oic_pbatch', 'oic_ptrip',
        'vehicle_code','do_sap','item','material_code','quantity','material_text','smo_order_id'];
    /**
     * @param string|null $scenario
     * @return array
     */
    public function getRules(string $scenario = null): array
    {
        return [
        ];
    }

    public function getLablesProduct($select_date, $area_id)
    {
        $query_all_product = 'select distinct smo_order_detail.material_code, smo_order_detail.material_text
                                     from smo_order_detail inner join car_in_out_history
                                     on smo_order_detail.in_out_history_id = car_in_out_history.id
                                     where DATE_FORMAT(checkin_time,?) = ? ';
        if ($area_id){
            $query_all_product .= ' AND area_id ='. $area_id;
        }
        $sql_format_date_hour ='%Y-%m-%d';
        $sql_data = $this->db->query($query_all_product,[$sql_format_date_hour, $select_date])->getResult('array');
        return $sql_data;
    }


}