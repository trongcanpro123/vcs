<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;
use phpDocumentor\Reflection\Types\This;

/**
 * Class ContentCategoryModel
 * @package App\Models
 *
 * @property string $title
 * @property string $slug
 * @property int $category_id
 * @property string $intro
 * @property string $content
 * @property string $image
 * @property string $material
 * @property string $guarantee
 * @property int $price
 * @property int $discount
 * @property int $is_lock
 */
class PumpThroadModel extends BaseModel
{
    protected $table = 'pump_throad';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;

    protected $allowedFields = ['id', 'area_id', 'throad_name', 'thread_pararell_id', 'product_type_id', 'wattage', 'display_on',
        'active', 'pump_system_id', 'updated_by', 'created_by'
    ];


    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    const LOGIN_KEY = 'PUMP_THROAD';


    public function getRules(string $scenario = null): array
    {
        return [
            'throad_name' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Họng bơm không được để trống',
                ]
            ],
            'wattage' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Công suất không được để trống',
                ]
            ],
        ];
    }

    public function select_pump_system()
    {
        $pump_system_db = $this->db->query('SELECT `pump_name` FROM pump_system WHERE id = ?', [$this->pump_system_id])->getRow();
        if ($pump_system_db) {
            return $pump_system_db->pump_name;
        }
        return '';
    }

    public function select_pump_product_type($product_type_id)
    {
        return $this->db->query('SELECT * FROM pump_product_type WHERE id = ?', [$product_type_id])->getRow();

    }

    public function get_list_throad_alert($area_id, $display_on){
        $query = 'SELECT thr.*,pb.`pump_name`, prd.`product_name` FROM `pump_throad` AS thr LEFT JOIN `pump_system` AS pb
                    ON thr.`pump_system_id` = pb.`id`
                    LEFT JOIN `pump_product_type` AS prd ON thr.`product_type_id` =  prd.`id`
                    WHERE thr.`area_id` = ?  
                    AND thr.`display_on` = ?
                    and thr.active = 1
                    ORDER BY pb.`pump_name`, thr.`throad_name`';
        return $this->db->query($query,[$area_id,$display_on])->getResultArray();
    }

    public function select_pump_throad_system($pump_system_id, $area_id)
    {
        return $this->db->query('SELECT * FROM pump_throad WHERE pump_system_id = ? AND area_id = ?', [$pump_system_id, $area_id])->getResultArray();
    }

    public function select_pump_throad_system_update($pump_system_id, $area_id)
    {

        return $this->db->query('SELECT * FROM pump_throad  WHERE pump_system_id = ? AND area_id = ? AND NOT id = ?', [$pump_system_id, $area_id, $this->getPrimaryKey()])->getResultArray();
    }


}
