<?php namespace App\Controllers;

use App\Helpers\SettingHelper;
use App\Models\CarInOutHistoryModel;
use App\Models\CronModel;
use App\Models\TgbxOrderDetailModel;
use App\Models\TgbxOrderDetailSortModel;
use App\Models\VoiceSortModel;
use DateTime;
use SoapClient;
use sqlsrv_connect;
use \phpseclib\Net\SSH2;

require __DIR__ . '/../../vendor/autoload.php';

//use Google_Client;
//use Google_Service_Gmail;
//use Google_Service_Gmail_Message;

class Cron extends BaseController
{
    public function scan_update_item_status()
    {
        var_dump(date_default_timezone_get());
        var_dump(new DateTime());
        if ($this->is_processing_status(__FUNCTION__)) {
            return;
        }
        $this->update_process_status(__FUNCTION__, 1);

        $list_in_out = (new CarInOutHistoryModel())->where('ticket', 1)
            ->where('checkout_time IS NULL')
            ->findAll();

        $setting = (new SettingHelper());
        if ($list_in_out) {
            foreach ($list_in_out as $in_out) {
                $area_code = '';
                if ($in_out->area_id == '1') {
                    $area_code = 'nghi_huong';
                }
                if ($in_out->area_id == '2') {
                    $area_code = 'ben_thuy';

                }
                $checkout_order_status_done = true;
                $list_tgbx_detail = (new TgbxOrderDetailModel())->where('in_out_id', $in_out->id)->findAll();
                if ($list_tgbx_detail) {
                    foreach ($list_tgbx_detail as $tgbx) {

                        $trang_thai = $setting->api_tdh_check_status($in_out->car_number, $area_code, $tgbx->ma_lenh, $tgbx->ngay_xuat, $tgbx->ma_hang_hoa);
                        var_dump('quet', $area_code, $tgbx->ma_lenh, $tgbx->ngay_xuat, $trang_thai);
                        if ($trang_thai == 'dang_xuat') {
                            (new TgbxOrderDetailModel())->update($tgbx->id, [
                                'tdh_status' => $trang_thai,
                                'check_time' => (new DateTime())->format('Y-m-d H:i:s'),
                            ]);
                            $checkout_order_status_done = $checkout_order_status_done && false;
                            $voice_sort = (new VoiceSortModel())->where('tgbx_detail_id', $tgbx->id)->first();
                            if ($voice_sort) {
                                (new VoiceSortModel())->delete($voice_sort->id);
                            }
                        }
                        if ($trang_thai == 'ket_thuc') {
                            (new TgbxOrderDetailModel())->update($tgbx->id, [
                                'tdh_status' => $trang_thai,
                                'check_time' => (new DateTime())->format('Y-m-d H:i:s'),
                            ]);
                            //remove sort table
                            $sort = (new TgbxOrderDetailSortModel())->where('tgbx_detail_id', $tgbx->id)->first();
                            if ($sort) {
                                (new TgbxOrderDetailSortModel())->delete($sort->id);
                            }

                            $voice_sort = (new VoiceSortModel())->where('tgbx_detail_id', $tgbx->id)->first();
                            if ($voice_sort) {
                                (new VoiceSortModel())->delete($voice_sort->id);
                            }
                        } else {
                            (new TgbxOrderDetailModel())->update($tgbx->id, [
                                'check_time' => (new DateTime())->format('Y-m-d H:i:s'),
                            ]);
                            $checkout_order_status_done = $checkout_order_status_done && false;
                        }
                    }

                    if ($checkout_order_status_done) {
                        (new CarInOutHistoryModel())->update($in_out->id,
                            [
                                'checkout_order_status' => 1,
                            ]
                        );
                    }
                }

            }

        }

        $this->update_process_status(__FUNCTION__, 0);

    }

    public function is_processing_status($process_name)
    {
        $process = (new CronModel())->where('process_name', $process_name)->first();
        if ($process) {
            return $process->is_processing ? true : false;
        }
        return false;
    }

    public function update_process_status($process_name, $status)
    {
        $process = (new CronModel())->where('process_name', $process_name)->first();
        if (!$process) {
            (new CronModel())->insert(['process_name' => $process_name, 'is_processing' => 1]);
        } else {
            (new CronModel())->update($process->id, ['is_processing' => $status]);
        }
    }

    public function reset_camera_nghi_huong()
    {

        if ($this->is_processing_status(__FUNCTION__)) {
            return;
        }
        $this->update_process_status(__FUNCTION__, 1);

        $ssh = new SSH2('10.6.10.10');
        if (!$ssh->login('root', 'dvcs@2021')) {
            exit('Login Failed');
        }
        echo $ssh->exec('/var/www/html/nms/restart.sh');

        $this->update_process_status(__FUNCTION__, 0);
    }

    public function reset_camera_ben_thuy()
    {

        if ($this->is_processing_status(__FUNCTION__)) {
            return;
        }
        $this->update_process_status(__FUNCTION__, 1);

        $ssh = new SSH2('10.6.20.10');
        if (!$ssh->login('root', 'dvcs@2021')) {
            exit('Login Failed');
        }
        echo $ssh->exec('/var/www/html/nms/restart.sh');

        $this->update_process_status(__FUNCTION__, 0);
    }
    public function process_sms()
    {
        $data_post = $this->request->getPost();
        $serverName_benthuy = "10.6.20.5,1433";
        $serverName_nghihuong = "10.6.10.5,1433";
        $uid = "sa";
        $pwd = "123321";
        $databaseName = "Petro_int";

        $connectionInfo = array("UID" => $uid,
            "PWD" => $pwd,
            "Database" => $databaseName);
        /* conect database_smo */
        $serverName_smo = "10.6.8.15,7788";
        $uid_smo = "sa";
        $pwd_smo = "Abc@123456";
        $databaseName_smo = "SMO_PRODUCT";

        $connectionInfo_smo = array("UID" => $uid_smo,
            "PWD" => $pwd_smo,
            "Database" => $databaseName_smo);
        /* conect database_smo */

        /* conect my_database*/
        $my_serverName = "localhost, 3306";
        $my_uid = "dvcs";
        $my_pwd = "ad123123";
        $my_databaseName = "xangdau_db";

        $my_connectionInfo = array("UID" => $my_uid,
            "PWD" => $my_pwd,
            "Database" => $my_serverName);
        /* conect my_database */

        /* Connect using SQL Server Authentication. */
        $conn_benthuy = sqlsrv_connect($serverName_benthuy, $connectionInfo);
        $conn_nghihuong = sqlsrv_connect($serverName_nghihuong, $connectionInfo);
        $conn_smo = sqlsrv_connect($serverName_smo, $connectionInfo_smo);
        $my_conn = sqlsrv_connect($my_serverName, $my_connectionInfo);

        $checkout_ai_car_number = $data_post['checkout_ai_car_number'];
        $area_id = $data_post['area_id'];
        $date = date('Y-m-j');
        $newdate = strtotime('-1 day', strtotime($date));
        $newdate = date('Y-m-j', $newdate);
        $nowDate = date('Y/m/d H:i:s');
        $tsql = "SELECT SoLenh,NgayXuat FROM tblLenhXuatE5 FROM MaPhuongTien = $checkout_ai_car_number and NgayXuat In ($date, $newdate) and Status = 5;";

        if ($area_id == 1) {
            $stmt = sqlsrv_query($conn_nghihuong, $tsql);
        } else {
            $stmt = sqlsrv_query($conn_benthuy, $tsql);
        }

        if ($stmt) {
            echo "Statement executed.<br>\n";
        } else {
            echo "Error in statement execution.\n";
            die(print_r(sqlsrv_errors(), true));
        }
        while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
            echo "Col1: " . $row[0] . "\n";
            echo "Col2: " . $row[1] . "\n";
            echo "Col3: " . $row[2] . "<br>\n";
            echo "-----------------<br>\n";
        }
        for ($i = 0; $i < sqlsrv_num_rows($stmt); $i++) {
            $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC, $i);
            $query_phone = "EXEC dbo.GET_PHONENUMBER @DoSAP = $row[0], @VehicleNumber = '';";
            $check_solenh = "SELECT COUNT($row) FROM sms_log_detail";
            $phone = sqlsrv_query($conn_smo, $query_phone);
            $Content = '';
            $check;
            if ($camera_id == 1) {
                $check = sqlsrv_query($conn_nghihuong, $check_solenh);
                while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC)) {
                    if ($row[0] == 0 || $row[0] == null) {
                        $check = 0;
                    }
                }
                $Content = "Kho Nghi Huong thong bao: Don hang co lenh xuat $row[0] ngay $row[1] da duoc xuat hang, xe xitec $checkout_ai_car_number da roi khoi kho luc $nowDate '. Tran trong!";
            } else {
                $check = sqlsrv_query($conn_benthuy, $check_solenh);
                while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC)) {
                    if ($row[0] == 0) {
                        $check = 0;
                    }
                }
                $Content = "Kho Ben Thuy thong bao: Don hang co lenh xuat $row[0] ngay $row[1] da duoc xuat hang, xe xitec $checkout_ai_car_number da roi khoi kho luc $nowDate '. Tran trong!";
            }
            if ($check == 0) {
                $phone = '8481989893';
                $requestParams = array(
                    'User' => "smsbrand_xangdauna",
                    'Password' => "xd@258369",
                    'CPCode' => "XANGDAUNA",
                    'RequestID' => "1",
                    'UserID' => $phone,
                    'ReceiverID' => $phone,
                    'ServiceID' => "CtyXdauN.an",
                    'CommandCode' => "bulksms",
                    'Content' => $Content,
                    'ContentType' => "F",
                );
                $client = new SoapClient('http://ams.tinnhanthuonghieu.vn:8009/bulkapi?wsdl');
                $response = $client->wsCpMt($requestParams);
                if ($response == 1) {
                    $sql = "INSERT INTO sms_log_detail (so_lenh,
                    vehicle_number,
                    phone_sender,
                    phone_receive,
                    sms_send_time,
                    sms_status,
                    sms_result_code,
                    sms_result_mesage,
                    sms_content)
                    VALUES ($row[0], $checkout_ai_car_number, $phone, $phone, $nowDate, 1, $response, '', $Content)";
                    if ($my_conn->query($sql) === true) {
                        echo "New record created successfully";
                    } else {
                        echo "Error: " . $sql . "<br>" . $my_conn->error;
                    }
                    return json_encode([
                        'status' => 'OK',
                        'test_id' => $checkout_ai_car_number,
                    ]);
                } else {
                    $sql = "INSERT INTO sms_log_detail (so_lenh,
                    vehicle_number,
                    phone_sender,
                    phone_receive,
                    sms_send_time,
                    sms_status,
                    sms_result_code,
                    sms_result_mesage,
                    sms_content)
                    VALUES ($row[0], $checkout_ai_car_number, $phone, $phone, $nowDate, 0, $response, '', $Content)";
                    if ($my_conn->query($sql) === true) {
                        echo "New record created successfully";
                    } else {
                        echo "Error: " . $sql . "<br>" . $my_conn->error;
                    }
                    return json_encode([
                        'status' => 'Fail',
                        'test_id' => $checkout_ai_car_number,
                    ]);
                }
            }
        }
        sqlsrv_close($conn_smo);
        sqlsrv_close($my_conn);
        sqlsrv_close($conn_nghihuong);
        sqlsrv_close($conn_benthuy);
    }
}
