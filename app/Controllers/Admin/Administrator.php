<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Helpers\ArrayHelper;
use App\Helpers\SessionHelper;
use App\Models\AdministratorModel;
use App\Models\AreaModel;
use App\Models\CarInOutHistoryModel;
use App\Models\SmoOrderDetailModel;
use App\Models\TgbxOrderDetailModel;
use App\Models\VoiceOptionModel;
use CodeIgniter\Model;
use App\Models\AreaCameraConfigModel;
use DateTime;


class Administrator extends BaseController
{
    /**
     * @return string
     */
    public function index()
    {

        if ($this->check_permission_for('lanh_dao')){
            return $this->response->redirect($this->check_permission_for('lanh_dao'));
        }

        $model = (new AdministratorModel())
            ->whereNotIn('type_user', ['admin']);

        return $this->render('administrator/index', [
            'models' => $model->paginate(10),
            'pager' => $model->pager
        ]);
    }

    /**
     * @return \CodeIgniter\HTTP\Response|string
     */
    public function create()
    {
        if ($this->check_permission_for('lanh_dao')){
            return $this->response->redirect($this->check_permission_for('lanh_dao'));
        }

        /** @var AdministratorModel $model */
        $model = new AdministratorModel();
        $area = (new AreaModel())->findAll();
        if ($this->isPost() && $this->validate($model->getRules())) {
            try {
                $model = $model->loadAndSave($this->request, function ($request, $data) {
                    if (($pwd = ArrayHelper::getValue($data, 'password')) !== null) {
                        $data['password_hash'] = AdministratorModel::createPasswordHash($pwd);
                        unset($data['password']);
                    }
                    $data['type'] = AdministratorModel::TYPE_ADMIN;
                    return $data;
                });

                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'success',
                    'message' => 'Tạo mới thành công'
                ]);
                return $this->response->redirect(route_to('administrator'));
            } catch (\Exception $ex) {
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => "Tên đăng nhập đã được sử dụng"
                ]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'area' => $area,
            'validator' => $this->validator,
            'exception'=>$ex
        ]);
    }

    /**
     * @param $id
     * @return \CodeIgniter\HTTP\Response|string
     */
    public function update($id)
    {
        if ($this->check_permission_for('lanh_dao')){
            return $this->response->redirect($this->check_permission_for('lanh_dao'));
        }

        /** @var AdministratorModel $model */
        $model = (new AdministratorModel())->find($id);
        $area = (new AreaModel())->findAll();
        if (!$model) {
            return $this->renderError();
        }

        if ($this->isPost() && $this->validate($model->getRules('update'))) {
            try {
                $model = $model->loadAndSave($this->request, function ($request, $data) {
                    $data['type'] = AdministratorModel::TYPE_ADMIN;
                    return $data;
                });

                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'info',
                    'message' => 'Cập nhật thành công'
                ]);
                return $this->response->redirect(route_to('administrator'));
            } catch (\Exception $ex) {
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => $ex->getMessage()
                ]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'area' => $area,
            'validator' => $this->validator
        ]);
    }

    public function password($id)
    {
        /** @var AdministratorModel $model */
        $model = (new AdministratorModel())->find($id);
        if (!$model) {
            return $this->renderError();
        }

        if ($this->isPost()) {
            try {
                $model = $model->loadAndSave($this->request, function ($request, $data) {
                    if (($pwd = ArrayHelper::getValue($data, 'password')) !== null) {
                        $data['password_hash'] = AdministratorModel::createPasswordHash($pwd);
                        unset($data['password']);
                    }
                    $data['type'] = AdministratorModel::TYPE_ADMIN;
                    return $data;
                });

                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'info',
                    'message' => 'Thay đổi mật khẩu thành công'
                ]);
                return $this->response->redirect(route_to('administrator'));
            } catch (\Exception $ex) {
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => $ex->getMessage()
                ]);
            }
        }

        return $this->render('reset_password', [
            'model' => $model,
            'validator' => $this->validator
        ]);
    }

    public function change_password()
    {
        /** @var AdministratorModel $model */
        $current_user = AdministratorModel::findIdentity();
        if (!$current_user) {
            return $this->renderError();
        }
        if ($this->isPost()) {
            try {

                $data_post = $this->request->getPost();

                if($current_user->password_hash != AdministratorModel::createPasswordHash($data_post['old_password'])){
                    SessionHelper::getInstance()->setFlash('ALERT', [
                        'type' => 'danger',
                        'message' => 'Mật khẩu cũ không đúng'
                    ]);
                    return $this->response->redirect(route_to('administrator_change_password'));
                }

                if($data_post['password'] != $data_post['retype_password']){
                    SessionHelper::getInstance()->setFlash('ALERT', [
                        'type' => 'danger',
                        'message' => 'Xác nhận lại lại mật khẩu chưa khớp'
                    ]);
                    return $this->response->redirect(route_to('administrator_change_password'));
                }

                $current_user = $current_user->loadAndSave($this->request, function ($request, $data) {
                    if (($pwd = ArrayHelper::getValue($data, 'password')) !== null) {
                        $data['password_hash'] = AdministratorModel::createPasswordHash($pwd);
                        unset($data['password']);
                    }
                    $data['type'] = AdministratorModel::TYPE_ADMIN;
                    return $data;
                });

                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'info',
                    'message' => 'Thay đổi mật khẩu thành công'
                ]);
                $current_user->logout();
                return $this->response->redirect(route_to('admin_login'));
            } catch (\Exception $ex) {
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => $ex->getMessage()
                ]);
            }
        }

        return $this->render('change_password', [
            'model' => $current_user,
            'validator' => $this->validator
        ]);
    }
    /**
     * @param $id
     * @return \CodeIgniter\HTTP\Response|string
     */
    public function delete($id)
    {
        /** @var AdministratorModel $model */
        if (!$this->isPost() || !($model = (new AdministratorModel())->find($id))) {
            return $this->renderError();
        }

        SessionHelper::getInstance()->setFlash('ALERT', [
            'type' => 'warning',
            'message' => 'Xoá thành công'
        ]);
        $model->delete($model->getPrimaryKey());
        return $this->response->redirect(route_to('administrator'));
    }


    public function lanh_dao_index()
    {
        if ($this->check_permission_for('lanh_dao')){
            return $this->response->redirect($this->check_permission_for('lanh_dao'));
        }

        $area = (new AreaModel())->findAll();
        $current_user = AdministratorModel::findIdentity();
        if($current_user->type_user == 'lanh_dao_kho'){
            $area = (new AreaModel())->where('id',$current_user->area_id)->findAll();
        }
        $camera_in = (new AreaCameraConfigModel())->where('area_id', $area[0]->id)->where('in_out', 'in')->orderBy('title', 'ASC')->findAll();
        $camera_out = (new AreaCameraConfigModel())->where('area_id', $area[0]->id)->where('in_out', 'out')->orderBy('title', 'ASC')->findAll();


        return $this->render('administrator/manager_index', [
            'camera_in' => $camera_in,
            'camera_out' => $camera_out,
            'area' => $area
        ]);
    }

    public function load_camera_by_area()
    {
        $request = $this->request->getGet();
        $area_id = $request['id'];
        $camera_in = (new AreaCameraConfigModel())->where('area_id', $area_id)->where('in_out', 'in')->orderBy('title', 'ASC')->findAll();
        $camera_out = (new AreaCameraConfigModel())->where('area_id', $area_id)->where('in_out', 'out')->orderBy('title', 'ASC')->findAll();
        $link_cam_in_play = [];
        $link_cam_out_play = [];
        if ($camera_in) {
            foreach ($camera_in as $key => $in) {
                $link_cam_in_play[$key]['link_play'] = $in->get_link_play();
                $link_cam_in_play[$key]['title'] = $in->title;
            }
        }
        if ($camera_out) {
            foreach ($camera_out as $key => $out) {
                $link_cam_out_play[$key]['link_play'] = $out->get_link_play();
                $link_cam_out_play[$key]['title'] = $out->title;
            }
        }

        echo json_encode(['camera_in' => $link_cam_in_play, 'camera_out' => $link_cam_out_play]);
    }

    //Chi tiết vào ra
    public function in_out_detail()
    {
        if ($this->check_permission_for('lanh_dao')){
            return $this->response->redirect($this->check_permission_for('lanh_dao'));
        }

        $models_Iod = (new CarInOutHistoryModel());
        $area_models = (new AreaModel())->orderBy('name', 'asc')->findAll();
        $data = $this->request->getGet();
        $area_id = $data['area_id'];
        $search = $data['car_number'];
        $start_time = $data['start_time'];
        $end_time = $data['end_time'];
        $param_search = [];
        // Filter By keyword

        if ($area_id) {
            $models_Iod->where('area_id', $area_id);
            $param_search['area_id'] = $area_id;
        } else {
            if ($area_models) {
                $models_Iod->where('area_id', $area_models[0]->id);
                $param_search['area_id'] = $area_models[0]->id;
            }

        }

        if ($search) {
            $models_Iod->like('car_number', $search);
            $param_search['car_number'] = $search;
        }
        if ($start_time) {
            $param_search['start_time'] = $start_time;
            $start_time = DateTime::createFromFormat('d-m-Y H:i:s', $start_time)->format('Y-m-d H:i:s');
            $models_Iod = $models_Iod->where('checkin_time >= ', $start_time);
        }
        if ($end_time) {
            $param_search['end_time'] = $end_time;
            $end_time = DateTime::createFromFormat('d-m-Y H:i:s', $end_time)->format('Y-m-d H:i:s');
            $models_Iod->where('checkin_time <= ', $end_time);

        }
        $models_Iod->where('checkin_order > ', -2);
        $models_Iod->orderBy('id', 'DESC');
        return $this->render('administrator/in_out_detail', [
            'param_search' => $param_search,
            'models' => $models_Iod->paginate(),
            'pager' => $models_Iod->pager,
            'validator' => $this->validator,
            'area_models' => $area_models
        ]);
    }

    public function report_car_detail()
    {
        if ($this->check_permission_for('lanh_dao')){
            return $this->response->redirect($this->check_permission_for('lanh_dao'));
        }
        $area_models = (new AreaModel())->orderBy('name', 'asc')->findAll();
        $data = $this->request->getGet();
        $area_id = $data['area_id'];
        $select_date = $data['select_date'];
        $param_search = [];

        if ($area_id) {
            $param_search['area_id'] = $area_id;
        }
        if ($select_date) {
            $param_search['select_date'] = $select_date;
            $select_date = DateTime::createFromFormat('d-m-Y', $select_date)->format('Y-m-d');
        } else {
            $select_date = (new DateTime())->format('d-m-Y');
            $param_search['select_date'] = $select_date;
            $select_date = DateTime::createFromFormat('d-m-Y', $select_date)->format('Y-m-d');
        }
        $report_result = (new AdministratorModel())->get_bc_xe_chi_tiet($area_id, $select_date);
        return $this->render('administrator/report_car_detail', [
            'area_models' => $area_models,
            'param_search' => $param_search,
            'report_result' => $report_result
        ]);
    }

    public function report_product_detail()
    {
        if ($this->check_permission_for('lanh_dao')){
            return $this->response->redirect($this->check_permission_for('lanh_dao'));
        }

        $area_models = (new AreaModel())->orderBy('name', 'asc')->findAll();
        $data = $this->request->getGet();
        $area_id = isset($data['area_id']) ? $data['area_id'] : 0;
        $select_date = $data['select_date'];
        $tab_menu = !empty($data['tab']) ? $data['tab'] : '1';
        $param_search = [];
        if ($area_id){
            $param_search['area_id'] = $area_id;
        }
        if ($select_date) {
            $param_search['select_date'] = $select_date;
            $select_date = DateTime::createFromFormat('d-m-Y', $select_date)->format('Y-m-d');
        } else {
            $param_search['select_date'] = DateTime::createFromFormat('d-m-Y', date('d-m-Y'))->format('d-m-Y');
        }

        $labels_result = (new TgbxOrderDetailModel())->create_dynamic_label_table($select_date, $area_id);

        $labels = [];
        foreach ($labels_result as $key => $item){
            $labels[$item['ma_hang_hoa']] = $item;
        }
        $labels_result = $labels;

        $report_result = (new AdministratorModel())->get_bc_sp_chi_tiet($area_id, $select_date, $labels_result);

        return $this->render('administrator/report_product_detail', [
            'labels_result' => $labels_result,
            'area_models' => $area_models,
            'tab_menu' => $tab_menu,
            'param_search' => $param_search,
            'report_result' => $report_result
        ]);
    }

    public function report_product_total()
    {
        if ($this->check_permission_for('lanh_dao')){
            return $this->response->redirect($this->check_permission_for('lanh_dao'));
        }

        $area_models = (new AreaModel())->orderBy('name', 'asc')->findAll();
        $data = $this->request->getGet();
  
        $area_id = isset($data['area_id']) ? $data['area_id'] : 0;
        $from_date =  $data['from_date'];
        $to_date = $data['to_date'];
        $tab_menu = !empty($data['tab']) ? $data['tab'] : '1';
        $param_search = [];

        $param_search['area_id'] = $area_id ? $area_id : '0';

        if ($from_date){
            $param_search['from_date'] = $from_date;
            $from_date = DateTime::createFromFormat('d-m-Y', $from_date)->format('Y-m-d');
        }else {
            $from_date = date_sub(date_create(date('d-m-Y')), date_interval_create_from_date_string('7 days'))->format('d-m-Y');
            $param_search['from_date'] = $from_date;
            $from_date = DateTime::createFromFormat('d-m-Y', $from_date)->format('Y-m-d');
        }

        if ($to_date){
            $param_search['to_date'] = $to_date;
            $to_date = DateTime::createFromFormat('d-m-Y', $to_date)->format('Y-m-d');
        }else{
            $to_date = DateTime::createFromFormat('d-m-Y', date('d-m-Y'))->format('d-m-Y');
            $param_search['to_date'] = $to_date;
            $to_date = DateTime::createFromFormat('d-m-Y', date('d-m-Y'))->format('Y-m-d');
        }
    

        $label_from_to = (new TgbxOrderDetailModel())->create_dynamic_label_total_table($from_date, $to_date, $area_id);

        $labels = [];
        foreach ($label_from_to as $key => $item){
            $labels[$item['ma_hang_hoa']] = $item;
        }

        $report_result = (new AdministratorModel())->get_bc_sp_tong_hop($area_id, $from_date, $to_date, $labels);
        $data_total = $report_result['data_total'];
        return $this->render('administrator/report_product_total', [
            'labels' => $labels,
            'tab_menu' => $tab_menu,
            'area_models' => $area_models,
            'param_search' => $param_search,
            'data_total' => $data_total,
            'report_result' => $report_result
        ]);
    }

    public function report_car_total()
    {
        if ($this->check_permission_for('lanh_dao')){
            return $this->response->redirect($this->check_permission_for('lanh_dao'));
        }

        $area_models = (new AreaModel())->orderBy('name', 'asc')->findAll();
        $data = $this->request->getGet();
        $area_id = $data['area_id'];
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $tab_menu = !empty($data['tab']) ? $data['tab'] : '1';
        $param_search = [];
        if (isset($data['area_id'])) {
            $param_search['area_id'] = $area_id;
        }
        if ($start_date) {
            $param_search['start_date'] = $start_date;
            $start_date = DateTime::createFromFormat('d-m-Y', $start_date)->format('Y-m-d');
        } else {
            $param_search['start_date'] = DateTime::createFromFormat('d-m-Y', date('d-m-Y'))->format('d-m-Y');
        }
        if ($end_date) {
            $param_search['end_date'] = $end_date;
            $end_date = DateTime::createFromFormat('d-m-Y', $end_date)->format('Y-m-d');
        } else {
            $param_search['end_date'] = DateTime::createFromFormat('d-m-Y', date('d-m-Y'))->format('d-m-Y');
        }

        $report_result = (new AdministratorModel())->get_bc_xe_tong_hop($area_id, $start_date ,$end_date);

        return $this->render('administrator/report_car_total', [
            'area_models' => $area_models,
            'param_search' => $param_search,
            'tab_menu' => $tab_menu,
            'report_result' => $report_result
        ]);
    }


    public function report_throad_total()
    {
        if ($this->check_permission_for('lanh_dao')){
            return $this->response->redirect($this->check_permission_for('lanh_dao'));
        }

        $area_models = (new AreaModel())->orderBy('name', 'asc')->findAll();
        $data = $this->request->getGet();
        $area_id = isset($data['area_id']) ? $data['area_id'] : 0;
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];
        $tab_menu = !empty($data['tab']) ? $data['tab'] : '1';
        $param_search = [];

        $param_search['area_id']  = $area_id ? $area_id : '0';

        if ($from_date){
            $param_search['from_date'] = $from_date;
            $from_date = DateTime::createFromFormat('d-m-Y', $from_date)->format('Y-m-d');
        }else {
            $from_date = date_sub(date_create(date('d-m-Y')), date_interval_create_from_date_string('7 days'))->format('d-m-Y');
            $param_search['from_date'] = $from_date;
            $from_date = DateTime::createFromFormat('d-m-Y', $from_date)->format('Y-m-d');
        }

        if ($to_date){
            $param_search['to_date'] = $to_date;
            $to_date = DateTime::createFromFormat('d-m-Y', $to_date)->format('Y-m-d');
        }else{
            $param_search['to_date'] = date('d-m-Y');
            $to_date = DateTime::createFromFormat('d-m-Y', date('d-m-Y'))->format('Y-m-d');
        }

        $label_from_to = (new TgbxOrderDetailModel())->create_dynamic_label_throad_table($from_date, $to_date, $area_id);

        $labels = [];
        foreach ($label_from_to as $key => $item){
            $labels[$item['throad_id']] = $item;
        }

        $report_result = (new AdministratorModel())->get_bc_sp_hong_tong_hop($area_id, $from_date, $to_date, $labels);
        $data_total = $report_result['data_total'];

        return $this->render('administrator/report_throad_total', [
            'labels' => $labels,
            'area_models' => $area_models,
            'param_search' => $param_search,
            'tab_menu' => $tab_menu,
            'data_total' => $data_total,
            'report_result' => $report_result
        ]);
    }
    public function report_throad_detail(){
        if ($this->check_permission_for('lanh_dao')){
            return $this->response->redirect($this->check_permission_for('lanh_dao'));
        }

        $area_models = (new AreaModel())->orderBy('name', 'asc')->findAll();
        $data = $this->request->getGet();
        $area_id = isset($data['area_id']) ? $data['area_id'] : 0;
        $select_date = $data['select_date'];
        $tab_menu = !empty($data['tab']) ? $data['tab'] : '1';
        $param_search = [];
        if ($area_id){
            $param_search['area_id'] = $area_id;
        }
        if ($select_date) {
            $param_search['select_date'] = $select_date;
            $select_date = DateTime::createFromFormat('d-m-Y', $select_date)->format('Y-m-d');
        } else {
            $param_search['select_date'] = DateTime::createFromFormat('d-m-Y', date('d-m-Y'))->format('d-m-Y');
        }
        $labels_result = (new TgbxOrderDetailModel())->create_dynamic_label_throad_name_table($select_date, $area_id);

        $labels = [];
        foreach ($labels_result as $key => $item){
            $labels[$item['throad_id']] = $item;
        }


        $report_result = (new AdministratorModel())->get_bc_hong_chi_tiet($area_id, $select_date, $labels);
        $data_total = $report_result['data_total'];


        return $this->render('administrator/report_throad_detail', [
            'labels' => $labels,
            'area_models' => $area_models,
            'param_search' => $param_search,
            'tab_menu' => $tab_menu,
            'data_total' => $data_total,
            'report_result' => $report_result
        ]);
    }

    public function voice_option(){
        if ($this->check_permission_for('lanh_dao')){
            return $this->response->redirect($this->check_permission_for('lanh_dao'));
        }

        $model = (new VoiceOptionModel())->first();
        if (!$model) {
            return $this->renderError();
        }

        if ($this->isPost() && $this->validate($model->getRules('update'))) {
            try {
                $model = $model->loadAndSave($this->request, function ($request, $data) {
                    return $data;
                });

                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'info',
                    'message' => 'Cập nhật thành công'
                ]);
                return $this->response->redirect(route_to('ld_voice_option'));
            } catch (\Exception $ex) {
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => $ex->getMessage()
                ]);
            }
        }

        return $this->render('administrator/voice_option', [
            'model' => $model,
        ]);
    }
}