<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Helpers\ArrayHelper;
use App\Helpers\SessionHelper;
use App\Models\AdministratorModel;
use App\Models\AreaModel;
use App\Models\CarInOutHistoryModel;
use App\Models\PumpSystemModel;
use CodeIgniter\Model;
use CodeIgniter\Session\Session;
use DateTime;
class PumpSystem extends BaseController
{
    /**
     * @return string
     */
    public function index()
    {
        if ($this->check_permission_for('nhan_vien')){
            return $this->response->redirect($this->check_permission_for('nhan_vien'));
        }

        $session = Session();
        $user_id = $session->get('PANDA_ADMIN');
        $model_admin = (new AdministratorModel())->select_area_admin_id($user_id['id']);
        $area_id = $model_admin->area_id;
        $model = (new PumpSystemModel())
        ->where('area_id',$area_id);

        return $this->render('pump_system/index', [
            'models' => $model->paginate(),
            'pager' => $model->pager
        ]);
    }

    public function create()
    {
        if ($this->check_permission_for('nhan_vien')){
            return $this->response->redirect($this->check_permission_for('nhan_vien'));
        }

        $model = new PumpSystemModel();
        $session = Session();
        $user_id = $session->get('PANDA_ADMIN');

        $model_area = (new AreaModel())->select_area($user_id['id']);


        if ($this->isPost() && $this->validate($model->getRules())) {
            try {
                $model->loadAndSave($this->request, function ($request, array $data) {
                    return $data;
                });

                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'success',
                    'message' => 'Thêm mới thành công'
                ]);

                return $this->response->redirect(route_to('nv_system'));
            } catch (\Exception $ex) {
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => $ex->getMessage()
                ]);
            }
        }
        return $this->render('pump_system/create', [
            'model' => $model,
            'model_area'=>$model_area,
            'validator' => $this->validator
        ]);
    }

    public function update($id)
    {
        if ($this->check_permission_for('nhan_vien')){
            return $this->response->redirect($this->check_permission_for('nhan_vien'));
        }

        $session = Session();
        $user_id = $session->get('PANDA_ADMIN');

        $model_area = (new AreaModel())->select_area($user_id['id']);
        $model = (new PumpSystemModel())->find($id);
        $model_name = $model->area_id ;

        if ($this->isPost() && $this->validate($model->getRules())) {
            try {
                $model->loadAndSave($this->request, function ($request, array $data) {
                    return $data;
                });

                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'success',
                    'message' => 'Sửa thành công'
                ]);

                return $this->response->redirect(route_to('nv_system'));
            } catch (\Exception $ex) {
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => $ex->getMessage()
                ]);
            }
        }
        return $this->render('pump_system/update', [
            'model' => $model,
            'model_area'=>$model_area,
            'model_name'=>$model_name,
            'validator' => $this->validator
        ]);
    }

    public function delete($id){
        if ($this->check_permission_for('nhan_vien')){
            return $this->response->redirect($this->check_permission_for('nhan_vien'));
        }

        /** @var PumpSystemModel $model */
        if (!$this->isPost() || !($model = (new PumpSystemModel())->find($id))) {
            return $this->renderError();
        }

        SessionHelper::getInstance()->setFlash('ALERT', [
            'type' => 'warning',
            'message' => 'Xoá thành công'
        ]);
        $model->delete($model->getPrimaryKey());
        return $this->response->redirect(route_to('nv_system'));
    }

}