<?php

namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use App\Helpers\ArrayHelper;
use App\Helpers\SessionHelper;
use App\Helpers\SettingHelper;
use App\Helpers\StringHelper;

use App\Models\AreaCameraConfigModel;
use App\Models\CameraConfigModel;

use App\Models\CarInOutHistoryModel;

use App\Models\SmoOrderDetailModel;
use App\Models\SmoOrderModel;
use App\Models\TgbxOrderDetailSortModel;
use App\Models\VoiceSortModel;
use CodeIgniter\Model;
use App\Models\AdministratorModel;
use App\Models\DriverModel;
use App\Models\CarModel;
use CodeIgniter\HTTP\Files\UploadedFile;
use CodeIgniter\HTTP\Request;
use CodeIgniter\Session\Session;
use App\Models\SMSHistoryModel;
use DateTime;

class GateKeeper extends BaseController
{
    /**
     * @return string
     */
    public function index()
    {
        if ($this->check_permission_for('bao_ve')){
            return $this->response->redirect($this->check_permission_for('bao_ve'));
        }

        /** @var DriverModel $model */
        $model = new DriverModel();

        $data = $this->request->getGet();
        $driver_name = $data['driver_name'];
        $phone_number = $data['phone_number'];
        $param_search = [];

        // Filter By keyword

        // if (($keyword = $this->request->getGet('driver_name')) !== null) {
        //     $model = $model->like('driver_name', $keyword);
        // }

        // if (($keyword = $this->request->getGet('phone_number')) !== null) {
        //     $model = $model->like('phone_number', StringHelper::rewrite($keyword));
        // }

        if ($driver_name) {
            $model->like('driver_name', $driver_name);
            $param_search['driver_name'] = $driver_name;
        }

        if($phone_number){
            $model->like('phone_number', $phone_number);
            $param_search['phone_number'] = $phone_number;
        }


        return $this->render('driver/index', [
            'param_search' => $param_search,
            'models' => $model->paginate(),
            'pager' => $model->pager
        ]);
    }

    /**
     * @return \CodeIgniter\HTTP\Response|string
     */
    public function create($in_out_id='')
    {
        if ($this->check_permission_for('bao_ve')){
            return $this->response->redirect($this->check_permission_for('bao_ve'));
        }

        /** @var DriverModel $model */
        $model = new DriverModel();
        if ($this->isPost() && $this->validate($model->getRules())) {
            try {
                $identity = AdministratorModel::findIdentity();
                $data_post = $this->request->getPost();
                $data_post['created_by'] = $identity->id;

                if(MODE_TEST){
                    $fake_model = (new CarInOutHistoryModel())->where('id',1)->first();
                    $data_post['image_front'] = $fake_model->image_capture;
                    $data_post['image_left'] = $fake_model->image_capture;
                    $data_post['image_right'] = $fake_model->image_capture;

                }
                $data_save = $data_post;
                $setting = (new SettingHelper());

                if($data_save['image_front'] && (strpos($data_save['image_front'],'data') !== false) ){
                    $data_save['image_front'] = $setting->upload_base_64_to_file($data_save['image_front']);
                }
                if($data_save['image_left'] && (strpos($data_save['image_left'],'data') !== false) ){
                    $data_save['image_left'] = $setting->upload_base_64_to_file($data_save['image_left']);
                }
                if($data_save['image_right'] && (strpos($data_save['image_right'],'data') !== false) ){
                    $data_save['image_right'] = $setting->upload_base_64_to_file($data_save['image_right']);
                }

                $detect_car_driver = $setting->api_detect_driver(ROOTPATH . PUBLISH_FOLDER. $data_save['image_front']);
                if($detect_car_driver){
                    if($detect_car_driver->code == '200'){
                        $ai_driver_id = $detect_car_driver->data->user_id;
                        $ai_driver_image = API_AI_BASE_URL . $detect_car_driver->data->image_path;
                        SessionHelper::getInstance()->setFlash('ALERT', [
                            'type' => 'danger',
                            'message' => 'Hệ thống đã nhận diện tài xế này đã có'
                        ]);

                        if($in_out_id){
                            return $this->response->redirect(base_url('/quantri/xu-ly-check-in/'.$in_out_id));
                        }
                        return $this->response->redirect(route_to('driver'));

                    }
                }

                $driver_id = $model->insert($data_save,true);

                //ban 3 phat qua AI
                $setting = new SettingHelper();
                $file_path = $setting->save_base_64_to_file($data_post['image_front']);
                $setting->api_register_driver($driver_id,$file_path);
                $file_path_left = $setting->save_base_64_to_file($data_post['image_left']);
                $setting->api_register_driver($driver_id,$file_path_left);
                $file_path_right = $setting->save_base_64_to_file($data_post['image_right']);
                $setting->api_register_driver($driver_id,$file_path_right);

                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'success',
                    'message' => 'Thêm mới thành công'
                ]);

                if($in_out_id){
                    (new CarInOutHistoryModel())->update($in_out_id,[
                        'driver_image_detected' => $data_save['image_front'],
                       'driver_name_detected' => $driver_id,
                       'driver_name' => $data_save['driver_name']
                    ]);
                    return $this->response->redirect(base_url('/quantri/xu-ly-check-in/'.$in_out_id));

                }

                return $this->response->redirect(route_to('driver'));
            } catch (\Exception $ex) {
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => $ex->getMessage()
                ]);
            }
        }
        $camera_in = $this->getListCameraIn();
        return $this->render('driver/create', [
            'model' => $model,
            'camera_in' => $camera_in,
            'validator' => $this->validator,
            'in_out_id' => $in_out_id
        ]);
    }

    /**
     * @param $id
     * @return \CodeIgniter\HTTP\Response|string
     */
    public function update($id)
    {
        if ($this->check_permission_for('bao_ve')){
            return $this->response->redirect($this->check_permission_for('bao_ve'));
        }

        /** @var DriverModel $model */
        $model = (new DriverModel())->find($id);

        if (!$model) {
            return $this->renderError();
        }

        if ($this->isPost()) {
            try {

                $data_post = $this->request->getPost();
                $exist = (new DriverModel())->where('id <>',$id)->where('phone_number',$data_post['phone_number'] )->first();
                if($exist){
                    SessionHelper::getInstance()->setFlash('ALERT', [
                        'type' => 'danger',
                        'message' => 'Đã tồn tại số điện thoại này ở tài xế khác'
                    ]);
                    return $this->response->redirect('/quantri/driver/update/'.$id);
                }
                $setting = (new SettingHelper());

                if($data_post['image_front'] && (strpos($data_post['image_front'],'data') !== false) ){
                    $data_post['image_front'] = $setting->upload_base_64_to_file($data_post['image_front']);
                }
                if($data_post['image_left'] && (strpos($data_post['image_left'],'data') !== false) ){
                    $data_post['image_left'] = $setting->upload_base_64_to_file($data_post['image_left']);
                }
                if($data_post['image_right'] && (strpos($data_post['image_right'],'data') !== false) ){
                    $data_post['image_right'] = $setting->upload_base_64_to_file($data_post['image_right']);
                }
//                var_dump($data_post['image_left'] && (strpos($data_post['image_left'],'data') !== false)); die;
                $model->update($id,$data_post);

                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'success',
                    'message' => 'Cập nhật thành công'
                ]);

                return $this->response->redirect(route_to('driver'));
            } catch (\Exception $ex) {
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => $ex->getMessage()
                ]);
            }
        }

        $camera_in = $this->getListCameraIn();
        return $this->render('driver/update', [
            'model' => $model,
            'camera_in' => $camera_in,
            'readonly' => true,
            'validator' => $this->validator
        ]);
    }

    public function getListCameraIn()
    {
        $bao_ve = AdministratorModel::findIdentity();
        $camera_in = (new AreaCameraConfigModel())->where('area_id',$bao_ve->area_id)->where('in_out','in')->orderBy('title','ASC')->findAll();
        return $camera_in;
    }

    /**
     * @param $id
     * @return \CodeIgniter\HTTP\Response|string
     */
    public function delete($id)
    {
        if ($this->check_permission_for('bao_ve')){
            return $this->response->redirect($this->check_permission_for('bao_ve'));
        }

        /** @var DriverModel $model */
        if (!$this->isPost() || !($model = (new DriverModel())->find($id))) {
            return $this->renderError();
        }

        SessionHelper::getInstance()->setFlash('ALERT', [
            'type' => 'warning',
            'message' => 'Xoá thành công'
        ]);
        $model->delete($model->getPrimaryKey());
        $setting = new SettingHelper();
        $setting->api_delete_driver($model->getPrimaryKey());
        return $this->response->redirect(route_to('driver'));
    }

    public function in_out_manager()
    {
        if ($this->check_permission_for('bao_ve')){
            return $this->response->redirect($this->check_permission_for('bao_ve'));
        }

        $bao_ve = AdministratorModel::findIdentity();
        $camera_in = (new AreaCameraConfigModel())->where('area_id',$bao_ve->area_id)->where('in_out','in')->orderBy('title','ASC')->findAll();
        $camera_out = (new AreaCameraConfigModel())->where('area_id',$bao_ve->area_id)->where('in_out','out')->orderBy('title','ASC')->findAll();

        return $this->render('gate_keeper/in_out_manager',[
            'bao_ve' => $bao_ve,
            'camera_in' => $camera_in,
            'camera_out' => $camera_out,

        ]);
    }

    public function bv_create_car($in_out_id = '',$car_number_detected= '')
    {
        if ($this->check_permission_for('bao_ve')){
            return $this->response->redirect($this->check_permission_for('bao_ve'));
        }

        /** @var CarModel $model */
        $model = new CarModel();
        if ($this->isPost() && $this->validate($model->getRules())) {
            try {
                $data_post = $this->request->getPost();

                $identity = AdministratorModel::findIdentity();
                $model->loadAndSave($this->request, function (Request $request, array $data) use ($model, $identity) {
                    $data['car_number'] =  str_replace('-','', str_replace('.','',strtoupper($data['car_number'])) );
                    $data['created_by'] = $identity->id;
                    $setting = (new SettingHelper());

                    if($data['image_front'] && (strpos($data['image_front'],'data') !== false) ){
                        $data['image_front'] = $setting->upload_base_64_to_file($data['image_front']);
                    }
                    if($data['image_car'] && (strpos($data['image_car'],'data') !== false) ){
                        $data['image_car'] = $setting->upload_base_64_to_file($data['image_car']);
                    }
                    return $data;
                });
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'success',
                    'message' => 'Thêm mới thành công'
                ]);
                if($in_out_id){
                    (new CarInOutHistoryModel())->update($in_out_id,[
                       'car_number' => $data_post['car_number']
                    ]);
                    return $this->response->redirect(base_url('/quantri/xu-ly-check-in/'.$in_out_id));
                }
                return $this->response->redirect(route_to('bv_list_cars'));
            } catch (\Exception $ex) {
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => $ex->getMessage()
                ]);
            }
        }
        $camera_in = $this->getListCameraIn();
        return $this->render('gate_keeper/car_create', [
            'model' => $model,
            'camera_in' => $camera_in,
            'validator' => $this->validator,
            'in_out_id' => $in_out_id,
            'car_number_detected' => $car_number_detected
        ]);
    }
    public function bv_detect_car_number(){
        if ($this->isPost()){
            try {
                $data = $this->request->getPost();
                $setting = (new SettingHelper());
                if($data['image_front'] && (strpos($data['image_front'],'data') !== false) ){
                    $file_path = $setting->save_base_64_to_file($data['image_front']);
                    $detect_car_number = $setting->api_detect_car_number($file_path);
                    if($detect_car_number){
                        if($detect_car_number->code == '200'){
                            $ai_car_number = $detect_car_number->data->text;
                            return json_encode($ai_car_number);
                        }
                    }
                }


            } catch (\Exception $ex) {
                return json_encode('');
            }
        }
        return json_encode('');

    }
    public function bv_update_car($id)
    {
        if ($this->check_permission_for('bao_ve')){
            return $this->response->redirect($this->check_permission_for('bao_ve'));
        }

        /** @var CarModel $model */
        $model = (new CarModel())->find($id);
        if (!$model) {
            return $this->renderError();
        }
        if ($this->isPost()) {
            try {
                $data_post = $this->request->getPost();
                $setting = (new SettingHelper());

                if($data_post['image_front'] && (strpos($data_post['image_front'],'data') !== false) ){
                    $data_post['image_front'] = $setting->upload_base_64_to_file($data_post['image_front']);
                }
                if($data_post['image_car'] && (strpos($data_post['image_car'],'data') !== false) ){
                    $data_post['image_car'] = $setting->upload_base_64_to_file($data_post['image_car']);
                }
                $model->update($id,$data_post);
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'success',
                    'message' => 'Cập nhật thành công'
                ]);
                return $this->response->redirect(route_to('bv_list_cars'));
            } catch (\Exception $ex) {
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => $ex->getMessage()
                ]);
            }
        }

        $camera_in = $this->getListCameraIn();
        return $this->render('gate_keeper/car_update', [
            'model' => $model,
            'camera_in' => $camera_in,
            'readonly' => true,
            'validator' => $this->validator
        ]);
    }


    public function bv_delete_car($id)
    {
        if ($this->check_permission_for('bao_ve')){
            return $this->response->redirect($this->check_permission_for('bao_ve'));
        }

        /** @var CarModel $model */
        if (!$this->isPost() || !($model = (new CarModel())->find($id))) {
            return $this->renderError();
        }
        SessionHelper::getInstance()->setFlash('ALERT', [
            'type' => 'warning',
            'message' => 'Xoá thành công'
        ]);
        $model->delete($model->getPrimaryKey());
        return $this->response->redirect(route_to('bv_list_cars'));
    }


    public function in_out_history()
    {
        if ($this->check_permission_for('bao_ve')){
            return $this->response->redirect($this->check_permission_for('bao_ve'));
        }

        $bao_ve = AdministratorModel::findIdentity();
        /** @var CarModel $model */
        $model = new CarInOutHistoryModel();
        $data = $this->request->getGet();
        $driver_name = $data['driver_name'];
        $car_number = $data['car_number'];
        $start_time = $data['start_time'];
        $end_time = $data['end_time'];
        $checkin_order = isset($data['checkin_order']) ? true : false;
        $param_search = [];
        if ($driver_name) {
            $model->like('driver_name', $driver_name);
            $param_search['driver_name'] = $driver_name;
        }

        if($car_number){
            $model->like('car_number', $car_number);
            $param_search['car_number'] = $car_number;
        }

        if($start_time){
            $param_search['start_time'] = $start_time;
            $start_time = DateTime::createFromFormat('d-m-Y H:i:s', $start_time)->format('Y-m-d H:i:s');
            $model->where('checkin_time >= ',$start_time);
        }

        if ($end_time){
            $param_search['end_time'] = $end_time;
            $end_time = DateTime::createFromFormat('d-m-Y H:i:s', $end_time)->format('Y-m-d H:i:s');
            $model->where('checkin_time <= ',$end_time);
        }
        if($checkin_order){
            $model->where('checkin_order', 0);
            $param_search['checkin_order'] = 'checked';
        }
        $model->where('area_id', $bao_ve->area_id);
        $model->where('checkin_order > ', -2);
        $model->orderBy('id', 'desc');
        return $this->render('gate_keeper/in_out_history',[
            'param_search' => $param_search,
            'models' => $model->paginate(),
            'pager' => $model->pager
        ]);
    }
    public function history_sms()
    {
        if ($this->check_permission_for('bao_ve')){
            return $this->response->redirect($this->check_permission_for('bao_ve'));
        }

        $bao_ve = AdministratorModel::findIdentity();
        /** @var SMSHistoryModel $model */
        $model = new SMSHistoryModel();
        $param_search = [];
        if ($vehicle_number) {
            $model->like('vehicle_number', $vehicle_number);
            $param_search['vehicle_number'] = $vehicle_number;
        }
        if ($sms_send_time) {
            $model->like('sms_send_time', $sms_send_time);
            $param_search['sms_send_time'] = $sms_send_time;
        }
        $model->orderBy('id', 'desc');
        return $this->render('gate_keeper/history_sms',[
            'param_search' => $param_search,
            'models' => $model->paginate(),
            'pager' => $model->pager
        ]);
    }

    public function show_all_list_cars()
    {
        if ($this->check_permission_for('bao_ve')){
            return $this->response->redirect($this->check_permission_for('bao_ve'));
        }

        /** @var CarModel $model */
        $model = new CarModel();
        $data = $this->request->getGet();
        $car_number = $data['car_number'];
        $param_search = [];

        if ($car_number) {
            $model->like('car_number', $car_number);
            $param_search['car_number'] = $car_number;
        }
        return $this->render('gate_keeper/car_index',[
            'param_search' => $param_search,
            'models' => $model->paginate(),
            'pager' => $model->pager
        ]);
    }

    public function help()
    {
        return $this->render('gate_keeper/help');
    }

    public function insert_check_in_image(){
        $data_post = $this->request->getPost();
        $bao_ve  = AdministratorModel::findIdentity();
        $model_in_out = (new CarInOutHistoryModel());
        $history_id = 0;
        //xử  lý lưu base 64 lại thành file

        if(MODE_TEST){
            $fake_model = (new CarInOutHistoryModel())->where('id',1)->first();
            $data_post['image_base64'] = $fake_model->image_capture;
        }
        $image_base64 = $data_post['image_base64'];


        if($image_base64){
            $setting = (new SettingHelper());

            $file_path = $setting->save_base_64_to_file($image_base64);
            $ai_car_number ='';
            $ai_car_number_image ='';

            $detect_car_number = $setting->api_detect_car_number($file_path);
            if($detect_car_number){
                if($detect_car_number->code == '200'){
                    $ai_car_number = $detect_car_number->data->text;
                    $ai_car_number_image = API_AI_BASE_URL . $detect_car_number->data->image_path;

                }
            }

            $ai_driver_id ='';
            $ai_driver_image ='';
            $detect_car_driver = $setting->api_detect_driver($file_path);
            if($detect_car_driver){
                if($detect_car_driver->code == '200'){
                    $ai_driver_id = $detect_car_driver->data->user_id;
                    $ai_driver_image = API_AI_BASE_URL . $detect_car_driver->data->image_path;
                }
            }
            $driver_name = '';
            if($ai_driver_id){
                $driver = (new DriverModel())->where('id',$ai_driver_id)->first();
                if($driver){
                    $driver_name = $driver->driver_name;
                }
            }

            if($data_post['image_base64'] && (strpos($data_post['image_base64'],'data') !== false) ){
                $data_post['image_base64'] = $setting->upload_base_64_to_file($data_post['image_base64']);
            }

            //xử lý lưu database checkin
            $history_id = $model_in_out->insert([
                'image_capture' => $data_post['image_base64'],
                'car_number_image_detected' => $ai_car_number_image,
                'car_number_detected' => $ai_car_number,
                'driver_image_detected' => $ai_driver_image,
                'driver_name_detected' => $ai_driver_id,
                'car_number' => $ai_car_number,
                'driver_name' => $driver_name,
                'checkin_order' => -2,
                'checkin_time' => (new DateTime())->format('Y-m-d H:i:s'),
                'checkin_by' => $bao_ve->id,
                'area_id' => $bao_ve->area_id,
                'customer_type' => 'not_smo'
            ], true);

        }


        return json_encode($history_id);
    }

    public function process_check_in($id){

        $model = (new CarInOutHistoryModel())->where('id',$id)->first();
        $list_smo_info  = (new SmoOrderModel())->orderBy('id','ASC')->where('in_out_history_id',$id)->findAll();
        $list_qr_code = [];
        if($list_smo_info){
            foreach ($list_smo_info as $smo_order){
                $qr_code_item = new \stdClass();
                $qr_code_item->qr_code = $smo_order->do_sap;

                $smo_order_info = null;

                $smo_order_detail_info = [];

                if($smo_order->status){
                    $smo_order_info = new \stdClass();
                    $smo_order_info->PoCode = $smo_order->po_code;
                    $smo_order_info->DocDate = $smo_order->doc_date;
                    $smo_order_info->OicPBatch = $smo_order->oic_pbatch;
                    $smo_order_info->OicPtrip = $smo_order->oic_ptrip;
                    $smo_order_info->VehicleCode = $smo_order->vehicle_code;
                    $smo_order_info->DoSAP = $smo_order->do_sap;

                    $list_smo_detail = (new SmoOrderDetailModel())->where('smo_order_id',$smo_order->id)->orderBy('id','ASC')->findAll();
                    foreach ($list_smo_detail as $smo_order_detail){

                        $item = new \stdClass();
                        $item->PoCode = $smo_order_detail->po_code;
                        $item->DocDate = $smo_order_detail->doc_date;
                        $item->OicPBatch = $smo_order_detail->oic_pbatch;
                        $item->OicPtrip = $smo_order_detail->oic_ptrip;
                        $item->VehicleCode = $smo_order_detail->vehicle_code;
                        $item->DoSAP = $smo_order_detail->do_sap;

                        $item->Item = $smo_order_detail->item;
                        $item->MaterialCode = $smo_order_detail->material_code;
                        $item->Quantity = $smo_order_detail->quantity;
                        $item->MaterialText = $smo_order_detail->material_text;

                        $smo_order_detail_info[] = $item;

                    }

                }

                $qr_code_item->data =  new \stdClass();
                $qr_code_item->data->order_info = $smo_order_info;
                $qr_code_item->data->order_detail = $smo_order_detail_info;
                $list_qr_code[] = $qr_code_item;
            }

        }




        return $this->render('gate_keeper/checkin_process', [
            'model' => $model,
            'list_smo_info' => $list_smo_info,
            'list_qr_code' => json_encode($list_qr_code)
        ]);
    }

    public function  process_checkin_cancel($id){
        $model = (new CarInOutHistoryModel())->where('id',$id)->where('checkin_order <= ',0)->first();
        if($model){
            (new SmoOrderModel())->delete_all_qr_code_info($id);
            (new CarInOutHistoryModel())->delete($id);
            SessionHelper::getInstance()->setFlash('ALERT', [
                'type' => 'warning',
                'message' => 'Đã hủy thành công'
            ]);
        }else{
            SessionHelper::getInstance()->setFlash('ALERT', [
                'type' => 'danger',
                'message' => 'Không cho phép hủy'
            ]);
        }

        return $this->response->redirect(route_to('bv_in_out_manager'));
    }

    public function process_checkin_reject($id){
        $model = (new CarInOutHistoryModel())->where('id',$id)->where('checkin_order <= ',0)->first();
        if($model){
            (new CarInOutHistoryModel())->update($id,[
                'checkin_time' => null,
                'checkin_order' => '-1'
            ]);
            $data_post= $this->request->getPost();
            $this->save_qr_code_info($id,$data_post['list_qr_code']);
            SessionHelper::getInstance()->setFlash('ALERT', [
                'type' => 'success',
                'message' => 'Đã xử lý không cho vào'
            ]);
            echo 1;
           return ;
        }else{
            SessionHelper::getInstance()->setFlash('ALERT', [
                'type' => 'danger',
                'message' => 'Không cho phép xử lý'
            ]);
        }
        echo 0;
        return ;

    }

    public function process_checkin_not_set_order($id){
        $model = (new CarInOutHistoryModel())->where('id',$id)->where('checkin_order <= ',0)->first();
        if($model){
            (new CarInOutHistoryModel())->update($id,[
                'checkin_order' => '0'
            ]);
            $data_post= $this->request->getPost();
            $this->save_qr_code_info($id,$data_post['list_qr_code']);
            SessionHelper::getInstance()->setFlash('ALERT', [
                'type' => 'success',
                'message' => 'Đã xử lý không cấp số'
            ]);
            echo 1;
            return ;
        }else{
            SessionHelper::getInstance()->setFlash('ALERT', [
                'type' => 'danger',
                'message' => 'Không cho phép xử lý'
            ]);
        }
        echo 0;
        return ;
    }

    public function process_checkin_set_order($id){
        $bao_ve = AdministratorModel::findIdentity();

        $model = (new CarInOutHistoryModel())->where('id',$id)->first();
        $data_post= $this->request->getPost();


        //check exist qr code
        $exist_qr_code = $this->check_exist_qr_code_info($id,$data_post['list_qr_code'],$model);
        if($exist_qr_code){
            return json_encode(['code' => -1, 'info'=> $exist_qr_code ]);
        }

        if($model && $model->checkin_order <=0){
            $this->save_qr_code_info($id,$data_post['list_qr_code']);
        }
        $model = (new CarInOutHistoryModel())->where('id',$id)->first();
        if($model->checkin_order && $model->checkin_order > 0 ){
            return json_encode(['code' => '1', 'info' => $model->checkin_order]);
        }
        if($model->customer_type != 'not_smo'){
            $exist_smo_order = (new SmoOrderModel())->where('in_out_history_id',$id)->first();
            if(!$exist_smo_order){
                return json_encode(['code' => '0', 'info' => '']);
            }

        }
        if($model && $model->car_number && $model->driver_name ){
            $checkin_order = (new CarInOutHistoryModel())->set_order_number($id, $bao_ve->area_id);
            return json_encode(['code' => '1', 'info' => $checkin_order]);
        }

        return json_encode(['code' => '0', 'info' => '']);
    }

    public function process_checkout_check_car_number(){
        $data_post = $this->request->getPost();
        $bao_ve  = AdministratorModel::findIdentity();

        if(MODE_TEST){
            $fake_model = (new CarInOutHistoryModel())->where('id',1)->first();
            $data_post['image_base64'] = $fake_model->image_capture;
        }
        $image_base64 = $data_post['image_base64'];
        $camera_id =$data_post['camera_id'];


        $setting = (new SettingHelper());

        $file_path = $setting->save_base_64_to_file($image_base64);
        $ai_car_number ='';
        $ai_car_number_image ='';

        $detect_car_number = $setting->api_detect_car_number($file_path);
        if($detect_car_number){
            if($detect_car_number->code == '200'){
                $ai_car_number = $detect_car_number->data->text;
                $ai_car_number_image = API_AI_BASE_URL . $detect_car_number->data->image_path;

            }
        }

        $session = session();
        $session->set(PREFIX_CHECKOUT_CAMERA_IMG . $camera_id,$image_base64);
        $session->set(PREFIX_CHECKOUT_AI_IMG . $camera_id,$ai_car_number_image);
        $session->set(PREFIX_CHECKOUT_AI_CAR_NUMBER . $camera_id,$ai_car_number);

        $result = new \stdClass();
        if(!$ai_car_number){
          $result->in_out_id = 0;
          $result->ai_car_number= '';
          $result->ai_car_number_image = '';
          return  json_encode($result);
        }else{

            $result->ai_car_number= $ai_car_number;
            $result->ai_car_number_image = $ai_car_number_image;

            $in_out_id = (new CarInOutHistoryModel())->get_car_checkout_by_car_number($ai_car_number, $bao_ve->area_id);
            $result->in_out_id = $in_out_id;
            return  json_encode($result);
        }


    }
    public function process_checkout_check_info($camera_id = 0, $in_out_id = 0 ){

        $session = session();
        $checkout_image = $session->get(PREFIX_CHECKOUT_CAMERA_IMG . $camera_id);
//        $checkout_ai_image = $session->get(PREFIX_CHECKOUT_AI_IMG . $camera_id);
        $checkout_ai_car_number = $session->get(PREFIX_CHECKOUT_AI_CAR_NUMBER . $camera_id);

        $model_in_out = null;
        if($in_out_id){

            $model_in_out = (new CarInOutHistoryModel());
            $model_in_out = $model_in_out->where('id',$in_out_id)->first();

        }
        return $this->render('gate_keeper/checkout_process',[
            'checkout_image' => $checkout_image,
            'model' => $model_in_out,
            'camera_id' => $camera_id,
            'checkout_ai_car_number' => $checkout_ai_car_number
        ]);
    }

    public function process_checkout_reject($camera_id = 0, $in_out_id = 0 ){
        $session = session();
//        $checkout_image = $session->get(PREFIX_CHECKOUT_CAMERA_IMG . $camera_id);
//        $checkout_ai_image = $session->get(PREFIX_CHECKOUT_AI_IMG . $camera_id);
//        $checkout_ai_car_number = $session->get(PREFIX_CHECKOUT_AI_CAR_NUMBER . $camera_id);

        $model_in_out = null;
        if($in_out_id){
//            (new CarInOutHistoryModel())->update($in_out_id,[
//                'checkout_image' => $checkout_image,
//                'checkout_car_number_image_detected' => $checkout_ai_image,
//                'checkout_car_number_detected' => $checkout_ai_car_number
//            ]);

            $in_out_info = (new CarInOutHistoryModel())->where('id',$in_out_id)->first();
            $alert = null;
            if($in_out_info){
                $alert = (new \stdClass());
                $alert->message = 'Tài xế ' . $in_out_info->driver_name . ' không được phép ra';
                $alert->create_time = (new DateTime())->getTimestamp();

            }
            $session->set(PREFIX_CHECKOUT_ALERT. $camera_id,$alert);
            return json_encode(1);

        }
        return json_encode(null);
    }

    public function process_checkout_recheck_car_number_info(){
        $data_post = $this->request->getPost();
        $bao_ve  = AdministratorModel::findIdentity();
        if(!$bao_ve) {
            return json_encode(null);
        }
        $car_number =  strtoupper(trim($data_post['car_number']));
        $car_number = str_replace('-','',$car_number);
        $car_number = str_replace('.','',$car_number);
        $car_number = str_replace(' ','',$car_number);
        $in_out_id = (new CarInOutHistoryModel())->get_car_checkout_by_car_number($car_number, $bao_ve->area_id);
        if($in_out_id){
            SessionHelper::getInstance()->setFlash('ALERT', [
                'type' => 'success',
                'message' => 'Đã tìm thấy thông tin xe'
            ]);
            return  json_encode($in_out_id);

        }
        return json_encode(null);
    }

    public function process_checkout_done(){
        $data_post = $this->request->getPost();
        $bao_ve  = AdministratorModel::findIdentity();
        if(!$bao_ve) {
            return json_encode(null);
        }

        $camera_id = $data_post['camera_id'];
        $in_out_id = $data_post['in_out_id'];
        $checkout_note = $data_post['checkout_note'];
        $model = (new CarInOutHistoryModel())->where('id',$in_out_id)->first();
//        if($model->checkout_time){
//            return json_encode(0);
//        }

        $session = session();

        $checkout_image = $session->get(PREFIX_CHECKOUT_CAMERA_IMG . $camera_id);
        $setting = (new SettingHelper());
        if($checkout_image && (strpos($checkout_image,'data') !== false) ){
            $checkout_image = $setting->upload_base_64_to_file($checkout_image);
        }

        $checkout_ai_image = $session->get(PREFIX_CHECKOUT_AI_IMG . $camera_id);
        $checkout_ai_car_number = $session->get(PREFIX_CHECKOUT_AI_CAR_NUMBER . $camera_id);

        (new CarInOutHistoryModel())->update($in_out_id,[
            'checkout_image' => $checkout_image,
            'checkout_car_number_image_detected' =>$checkout_ai_image,
            'checkout_car_number_detected' => $checkout_ai_car_number,
            'checkout_car_number' => $model->car_number,
            'checkout_reason' => $checkout_note,
            'checkout_time' => (new DateTime())->format('Y-m-d H:i:s'),
            'checkout_by' => $bao_ve->id
        ]);

        (new TgbxOrderDetailSortModel())->delete_by_in_out_id($in_out_id);
        (new VoiceSortModel())->delete_by_in_out_id($in_out_id);

        $alert = new \stdClass();
        $alert->message = 'Tài xế <label style="color: black" >' . $model->driver_name . '</label> đủ điều kiện ra </br> Chúc may mắn!';
        $alert->create_time = (new DateTime())->getTimestamp();
        $session->set(PREFIX_CHECKOUT_ALERT. $camera_id,$alert);

        return json_encode(1);

    }



    public function show_window_checkin_order($id){
        $model = (new CarInOutHistoryModel())->where('id',$id)->first();
        return view("quantri/gate_keeper/checkin_order_print", [
            'model' =>$model,
        ]);
    }
    public function checkout_alert($camera_id){
        $session = session();

        $alert = $session->get(PREFIX_CHECKOUT_ALERT . $camera_id);
        if($alert){
            $ts1 = $alert->create_time;
            $ts2 = (new DateTime())->getTimestamp();
            $seconds_diff = $ts2 - $ts1;
            if($seconds_diff > CHECKOUT_ALERT_SECOND){
                $session->set(PREFIX_CHECKOUT_ALERT . $camera_id,null);
                $alert = null;
            }
        }

        return view("quantri/gate_keeper/checkout_alert", [
            'alert' =>$alert,
        ]);
    }
    public function get_smo_info(){
        $bao_ve = AdministratorModel::findIdentity();
        $area_code = $bao_ve->area_id == '1' ? 'nghi_huong' : 'ben_thuy';
        $setting = (new SettingHelper());
        $data_post = $this->request->getPost();
        $qr_code = $data_post['qr_code'];
        $in_out_id = $data_post['in_out_id'];
        $in_out = (new CarInOutHistoryModel())->where('id',$in_out_id)->first();
        if($in_out->checkin_order > 0){
            return json_encode(null);
        }

        if((new SmoOrderModel())->check_exits_smo($in_out_id,$qr_code)){
            return json_encode(-1);
        }

        $qr_code_info = $setting->api_smo_info($qr_code,$area_code);
        if(!$qr_code_info->order_info){
            return json_encode(-2);
        }

        $first_order = (new SmoOrderModel())->where('in_out_history_id', $in_out_id)->first();
        if($qr_code_info->order_info){
            $order_info = (array)($qr_code_info->order_info);
            if(!$first_order){

                if($in_out && $in_out->customer_type == 'smo' && $in_out->checkin_car_number_note){
                    (new CarInOutHistoryModel())->update($in_out->id,[
                        'car_number' => $order_info['VehicleCode']
                    ]);

                }
            }
            $order_info_id = (new SmoOrderModel())->insert([
                'in_out_history_id' => $in_out_id,
                'po_code' => $order_info['PoCode'],
                'doc_date' =>  $order_info['DocDate'],
                'oic_pbatch' =>  $order_info['OicPBatch'],
                'oic_ptrip' =>  $order_info['OicPtrip'],
                'vehicle_code' => $order_info['VehicleCode'],
                'do_sap' => $order_info['DoSAP'],
                'status' => 1
            ],true);

            (new CarInOutHistoryModel())->update($in_out->id,[
                'customer_type' => 'smo'
            ]);

            foreach($qr_code_info->order_detail as $smo_order_detail){
                $smo_order_detail = (array)$smo_order_detail;

                (new SmoOrderDetailModel())->insert([
                    'in_out_history_id' => $in_out_id,
                    'po_code' => $smo_order_detail['PoCode'],
                    'doc_date' =>  $smo_order_detail['DocDate'],
                    'oic_pbatch' =>  $smo_order_detail['OicPBatch'],
                    'oic_ptrip' =>  $smo_order_detail['OicPtrip'],
                    'vehicle_code' => $smo_order_detail['VehicleCode'],
                    'do_sap' => $smo_order_detail['DoSAP'],
                    'item' => $smo_order_detail['Item'],
                    'material_code' => $smo_order_detail['MaterialCode'],
                    'quantity' => $smo_order_detail['Quantity'],
                    'material_text' => $smo_order_detail['MaterialText'],
                    'smo_order_id' => $order_info_id
                ]);

            }
        }else{

            $order_info_id = (new SmoOrderModel())->insert([
                'in_out_history_id' => $in_out_id,
                'do_sap' => $qr_code,
                'status' => 0
            ],true);
        }
        return json_encode($qr_code_info);

    }

    public function delete_smo_info(){
        $data_post = $this->request->getPost();
        $qr_code = $data_post['qr_code'];
        $in_out_id = $data_post['in_out_id'];
        (new SmoOrderModel())->delete_smo_info($in_out_id,$qr_code);

    }

    public function save_qr_code_info($in_out_id, $list_qr_code){
        (new SmoOrderModel())->delete_all_qr_code_info($in_out_id);
        if($list_qr_code){
            foreach ($list_qr_code as $n => $qr_code_info){
                if($qr_code_info['data']['order_info']){
                    $order_info = $qr_code_info['data']['order_info'];
                    if($n == 0){

                        $in_out = (new CarInOutHistoryModel())->where('id',$in_out_id)->first();

                        if($in_out && $in_out->customer_type == 'smo' && $in_out->checkin_car_number_note){
                            (new CarInOutHistoryModel())->update($in_out->id,[
                                'car_number' => $order_info['VehicleCode']
                            ]);
                        }
                    }
                    $order_info_id = (new SmoOrderModel())->insert([
                        'in_out_history_id' => $in_out_id,
                        'po_code' => $order_info['PoCode'],
                        'doc_date' =>  $order_info['DocDate'],
                        'oic_pbatch' =>  $order_info['OicPBatch'],
                        'oic_ptrip' =>  $order_info['OicPtrip'],
                        'vehicle_code' => $order_info['VehicleCode'],
                        'do_sap' => $order_info['DoSAP'],
                        'status' => 1
                    ],true);
                    (new CarInOutHistoryModel())->update($in_out->id,[
                        'customer_type' => 'smo'
                    ]);
                    foreach($qr_code_info['data']['order_detail'] as $smo_order_detail){
                        (new SmoOrderDetailModel())->insert([
                            'in_out_history_id' => $in_out_id,
                            'po_code' => $smo_order_detail['PoCode'],
                            'doc_date' =>  $smo_order_detail['DocDate'],
                            'oic_pbatch' =>  $smo_order_detail['OicPBatch'],
                            'oic_ptrip' =>  $smo_order_detail['OicPtrip'],
                            'vehicle_code' => $smo_order_detail['VehicleCode'],
                            'do_sap' => $smo_order_detail['DoSAP'],
                            'item' => $smo_order_detail['Item'],
                            'material_code' => $smo_order_detail['MaterialCode'],
                            'quantity' => $smo_order_detail['Quantity'],
                            'material_text' => $smo_order_detail['MaterialText'],
                            'smo_order_id' => $order_info_id
                        ]);

                    }
                }else{
                    $order_info = $qr_code_info['data']['order_info'];
                    $order_info_id = (new SmoOrderModel())->insert([
                        'in_out_history_id' => $in_out_id,
                        'do_sap' => $qr_code_info['qr_code'],
                        'status' => 0
                    ],true);
                }
            }
        }
    }

    public function check_exist_qr_code_info($in_out_id, $list_qr_code, $in_out_model ){
        $smo_order = (new SmoOrderModel());
        $smo_order->delete_all_qr_code_info($in_out_id);
        if($list_qr_code){
            foreach ($list_qr_code as $qr_code_info){
                if($qr_code_info['data']['order_info']){
                    $order_info = $qr_code_info['data']['order_info'];
                    $check_exist_order = (new SmoOrderModel())->check_exits_smo($in_out_id,$order_info['DoSAP']);
                    if($check_exist_order){
                        return $order_info['DoSAP'];
                    }

                }
            }
        }
        return false;
    }

    public function save_car_number_note_info($in_out_id){

        $data_post = $this->request->getPost();
        $in_out = (new CarInOutHistoryModel())->where('id',$in_out_id)->first();
        $customer_type = 'smo';
        if($data_post['checkin_car_number_hand']){
            $customer_type = 'not_smo';
        }
        $car_number = $in_out->car_number;
        if($customer_type == 'not_smo'){
            $car_number = str_replace(' ','',strtoupper($data_post['checkin_car_number_hand'])) ;
            $car_number = str_replace('-','',$car_number);
            $car_number = str_replace('.','',$car_number);
        }
        if($customer_type == 'smo'){
            $first_smo_order = (new SmoOrderModel())->where('in_out_history_id',$in_out_id)
                ->where('vehicle_code IS NOT NULL')
                ->where('vehicle_code <> ','')->orderBy('id','ASC')->first();
            if($first_smo_order){
                $car_number = strtoupper($first_smo_order->vehicle_code);
            }
        }
        (new CarInOutHistoryModel())->update($in_out_id,[
            'checkin_car_number_hand' => str_replace(' ','',strtoupper($data_post['checkin_car_number_hand'])),
            'checkin_car_number_note' => $data_post['checkin_car_number_note'],
            'car_number' => $car_number ,
            'customer_type' => $customer_type
        ]);

        echo 1;
        return;
    }
    public function save_driver_note_info($in_out_id){
        $data_post = $this->request->getPost();



        (new CarInOutHistoryModel())->update($in_out_id,[
            'checkin_driver_hand' => $data_post['checkin_driver_hand'],
            'checkin_driver_note' => $data_post['checkin_driver_note'],
            'driver_name' => $data_post['checkin_driver_hand']
        ]);
        echo 1;
        return;
    }
}