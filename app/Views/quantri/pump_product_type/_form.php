<?php

use App\Helpers\Html;

/**
 * @var \App\Models\AdministratorModel $model
 */
?>
<div class="form-group">
    <label class="bmd-label-floating">Tên mặt hàng</label>
    <input type="text" name="product_name" value="<?=$model->product_name?>" autocomplete="off" class="form-control">
</div>
<div class="form-group">
    <label class="bmd-label-floating">Mã mặt hàng</label>
    <input type="text" name="product_type_code" value="<?=$model->product_type_code?>" autocomplete="off" class="form-control">
</div>
