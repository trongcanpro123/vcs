<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Kiểm soát vào ra';
?>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="margin-bottom: 0px">
                    <div class="card-header card-header-text card-header-info">
                        <div class="card-text">
                            <h6 class="card-title">Chọn camera</h6>
                        </div>
                    </div>

                    <div class="card-body" style="padding-top: 0px">
                        <div class="row">
                            <div class="col-md-2 text-right">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" id="check_cam_in" name="check_cam_in" type="checkbox" checked onchange="change_check_cam_in();" >
                                        Camera Vào
                                        <span class="form-check-sign"><span class="check"></span></span>
                                    </label>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="">
                                    <select class="form-control " placeholder="Chọn Camera Vào" data-style="btn btn-link" id="select_camera_in" onchange="change_play_cam_gate_in(this, 'videoElement_in')">
                                        <?php if($camera_in) {
                                            foreach ($camera_in as $camera){
                                                ?>
                                                <option value="<?= $camera->get_link_play() ?>" ><?= $camera->title ?></option>

                                                <?php
                                            }
                                        } ?>

                                    </select>
                                </div>
                                <div class="">
                                    <button id="btn_check_in" onclick="gate_keeper_checkin('videoElement_in')" type="button" class="btn btn-info">Kiểm tra xe vào</button>
                                    <a href="/quantri/driver/create" type="button" class="btn btn-primary " style="color: white">Đăng ký tài xế</a>
                                    <a href="/quantri/car/create" type="button" class="btn btn-rose" style="color: white">Đăng ký xe</a>
                                    <?php
                                        if($bao_ve->area_id == '1'){
                                    ?>
                                        <a href="/cron/reset_camera_nghi_huong" target="_blank" type="button" class="btn btn-success" style="color: white">
                                            <i class="material-icons">refresh</i>
                                        </a>

                                    <?php
                                        }else{
                                    ?>
                                            <a href="/cron/reset_camera_ben_thuy" target="_blank" type="button" class="btn btn-success" style="color: white">
                                                <i class="material-icons">refresh</i>

                                            </a>
                                    <?php
                                        }
                                    ?>

                                </div>
                            </div>
                            <div class="col-md-2 text-right">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" id="check_cam_out"  name="check_cam_out" type="checkbox" checked onchange="change_check_cam_out()" >
                                        Camera Ra
                                        <span class="form-check-sign"><span class="check"></span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="">
                                    <select class="form-control " placeholder="Chọn Camera Ra" data-style="btn btn-link" id="select_camera_out" onchange="change_play_cam_gate_out(this, 'videoElement_out')">
                                        <?php if($camera_out) {
                                            foreach ($camera_out as $camera){
                                                ?>
                                                <option data-camera_out_id ="<?= $camera->id ?>" value="<?= $camera->get_link_play() ?>" ><?= $camera->title ?></option>

                                                <?php
                                            }
                                        } ?>

                                    </select>
                                </div>
                                <div class="">
                                    <button id="btn_check_out" onclick="process_checkout_check_info('videoElement_out')" type="button" class="btn btn-info">Kiểm tra xe ra</button>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-6 div_camera_in ">
                <div class="card" style="margin-top: 20px">
                    <div class="card-body text-center">
                        <video width="100%" id="videoElement_in"></video>
                        <canvas style="display: none" id="canvas_videoElement_in" ></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-6 div_camera_out">
                <div class="card" style="margin-top: 20px">
                    <div class="card-body text-center">
                        <video width="100% text-center" id="videoElement_out"></video>
                        <canvas style="display: none" id="canvas_videoElement_out" ></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var is_on_change = false;
    window.onload = function() {

        change_play_cam_gate_in($('#select_camera_in'), 'videoElement_in');
        change_play_cam_gate_out($('#select_camera_out'), 'videoElement_out');
        // setInterval(refresh_cam, 60*1000);

        document.addEventListener("visibilitychange", visibilitychange);

    };

    function refresh_cam(){
        change_play_cam_gate_in($('#select_camera_in'), 'videoElement_in');
        change_play_cam_gate_out($('#select_camera_out'), 'videoElement_out');
    }

    function visibilitychange (evt) {
        if(is_on_change) return;
        console.log('refresh');
        is_on_change = true;
        refresh_cam();
        is_on_change = false;
    }
</script>