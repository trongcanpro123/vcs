<?php
use App\Helpers\Html;
/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectModel $model
 * @var \CodeIgniter\Validation\Validation $validator
 */
$this->title = 'Cập nhật Quản trị viên';
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-info flex-align">
                <div>
                    <h4 class="card-title"><?= $this->title ?></h4>
                </div>

            </div>
            <div class="card-body">
                <form action="<?= route_to('administrator_update', $model->getPrimaryKey()) ?>" method="post"
                      enctype="multipart/form-data">

                    <?= $this->import('_form_edit', ['model' => $model]) ?>

                    <?php if ($validator): ?>
                        <div class="alert alert-danger" style="margin-top: 32px;">
                            <ul style="margin: 0; padding-left: 16px;">
                                <?php foreach ($validator->getErrors() as $error): ?>
                                    <li><?= Html::decode($error) ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <div class="card-bottom-actions">
                        <div class="flex-row" id="add-holder"></div>
                        <a href="<?= route_to('administrator') ?>"  class="btn btn-round">Huỷ</a>
                        <button class="btn btn-info btn-round" type="submit">Lưu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>