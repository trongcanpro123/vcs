<?php

use App\Helpers\Html;

/**
 * @var \App\Models\AdministratorModel $model
 */
?>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="bmd-label-floating">Họ và tên</label>
            <?= Html::textInput('full_name', $model->full_name, [
                'autocomplete' => 'off',
                'class' => 'form-control',
                'autofocus' => true,
            ]) ?>
        </div>
        <div class="form-group">
            <label class="bmd-label-floating">Chọn kho</label>
            <select class="selectpicker form-control" name="area_id">
                <?php if ($area): ?>
                    <?php foreach ($area as $ar): ?>
                        <option <?php if($model->area_id == $ar->id ){ echo 'selected'; } ?>  value="<?=$ar->id?>"><?=$ar->name?></option>
                    <?php endforeach; ?>
                <?php endif ?>


            </select>
        </div>

    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="bmd-label-floating">Tên đăng nhập</label>
            <input type="text" name="note" autocomplete="off" class="form-control" value="<?=$model->account_name?>" disabled>
        </div>
        <div class="form-group">
            <label class="bmd-label-floating">Chọn vai trò</label>
            <select class="selectpicker form-control" name="type_user">
                <option value="lanh_dao" <?php if($model->type_user == 'lanh_dao') echo 'selected' ?> >Lãnh đạo</option>
                <option value="lanh_dao_kho" <?php if($model->type_user == 'lanh_dao_kho') echo 'selected' ?> >Lãnh đạo kho</option>

                <option value="nhan_vien" <?php if($model->type_user == 'nhan_vien') echo 'selected' ?> >Nhân viên</option>
                <option value="bao_ve" <?php if($model->type_user == 'bao_ve') echo 'selected' ?> >Bảo vệ</option>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">

        <div class="form-group">
            <label class="bmd-label-floating">Ghi chú</label>
            <input type="text" name="note" autocomplete="off" class="form-control" value="<?=$model->note?>">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="bmd-label-floating">Trạng thai</label>
            <select class="selectpicker form-control" name="is_lock">
                <option value="0" <?php if($model->is_lock == 0) echo 'selected'; ?> >Hoạt động</option>
                <option value="1" <?php if($model->is_lock == 1) echo 'selected'; ?> >Khóa</option>
            </select>
        </div>
    </div>
</div>

