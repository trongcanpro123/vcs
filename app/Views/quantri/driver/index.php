<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Danh sách tài xế';
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-text card-header-info">
                <div class="card-icon">
                    <i class="material-icons">search</i>
                </div>
            </div>
            <div class="card-body ">
                <form action="<?= route_to('driver') ?>" method="GET">
                    <div class="row">
                        <div class="col-md-4">
                            <input placeholder="Tên tài xế" type="text" name="driver_name" autocomplete="off" class="form-control" autofocus=""
                                   value="<?= $param_search['driver_name'] ?>">
                        </div>
                        <div class="col-md-4">
                            <input placeholder="Số điện thoại di dộng" type="text" name="phone_number" autocomplete="off" class="form-control" autofocus=""
                                   value="<?= $param_search['phone_number'] ?>">
                        </div>
                        <div class="col-md-4">
                             <input type="submit" autocomplete="off" class="btn btn-info btn-round" value="Tìm kiếm">
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header card-header-info flex-align">
        <div>
            <h4 class="card-title"><?= $this->title ?></h4>
        </div>
        <a href="<?= route_to('admin_driver_create') ?>" class="btn btn-warning btn-round btn-sm">Thêm mới</a>
    </div>
    <div class="card-body table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Tài xế</th>
                    <th>Số di động</th>
                    <th style="text-align:center">Ảnh</th>
                    <th style="width: 120px">Hành động</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!$models || empty($models)): ?>
                    <tr>
                        <td colspan="100">
                            <div class="empty-block">
                                <img src="/images/no-content.jpg" alt="No content"/>
                                <h4>Không có nội dung</h4>
                                <a class="btn btn-info btn-round"
                                   href="<?= route_to('admin_driver_create') ?>">Thêm</a>
                            </div>
                        </td>
                    </tr>
                <?php else: ?>
                    <?php foreach ($models as $key => $model): ?>
                        <tr>
                            <td class="row-actions text-center"><?= ++$key ?></td>
                            <td><?= Html::decode($model->driver_name) ?></td>
                            <td><?= $model->phone_number ?></td>
                            <td style="text-align:center">
                                <img class="image_preview_snap_table" src="<?= $model->image_front ?>"/>
                            </td>
                            <td class="row-actions">
                                <?= Html::a('<i class="material-icons">edit</i>', ['admin_driver_update', $model->getPrimaryKey()], [
                                    'class' => ['btn', 'btn-info', 'btn-just-icon','btn-sm'],
                                    'title' => 'Sửa'
                                ]) ?>
                                <a href="<?= route_to('admin_driver_delete', $model->getPrimaryKey()) ?>"
                                   class="btn btn-danger btn-just-icon btn-sm" data-method="post"
                                   data-prompt="Bạn có chắc sẽ xoá đi mục này?">
                                    <i class="material-icons">delete</i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif ?>
                </tbody>
            </table>
            <?= $pager->links('default', 'default_cms') ?>
        </div>
 </div>
