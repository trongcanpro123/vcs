<?php
use App\Helpers\Html;
/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectCategoryModel $model
 * @var \CodeIgniter\Validation\Validation $validator
 */
$this->title = 'Tạo mới tài xế';
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-info flex-align">
                <div>
                    <h4 class="card-title"><?= $this->title ?></h4>
                </div>

            </div>
            <div class="card-body">
                <form action="<?= '/quantri/driver/create' . ($in_out_id?('/'.$in_out_id):'') ?>" method="post" enctype="multipart/form-data">

                    <?= $this->import('_form', ['model' => $model]) ?>

                    <?php if ($validator): ?>
                        <div class="alert alert-danger" style="margin-top: 32px;">
                            <ul style="margin: 0; padding-left: 16px;">
                                <?php foreach ($validator->getErrors() as $error): ?>
                                    <li><?= Html::decode($error) ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>

<!--                    <div style="text-align:center;margin-top:35px">-->
<!--                        <div class="flex-row" id="add-holder"></div>-->
<!--                        <a href="--><?//= route_to('driver') ?><!--" class="btn btn-round">Huỷ</a>-->
<!--                        <button style="margin-left:15px" class="btn btn-success btn-round" type="submit">Lưu</button>-->
<!--                    </div>-->
                </form>
            </div>
        </div>
    </div>
</div>