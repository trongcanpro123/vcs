<?php

use App\Helpers\Html;
use App\Helpers\StringHelper;
use App\Helpers\Widgets\NewsWidget;
use App\Models\SettingsModel;
use App\Helpers\SettingHelper;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\SliderModel[] $sliders
 * @var \App\Models\ProjectCategoryModel[] $projectCategories
 * @var \App\Models\CategoryModel[] $categories
 * @var \App\Models\TestimonialModel[] $testimonials
 * @var \App\Models\PartnerModel[] $partners
 * @var \App\Models\NewsModel[] $newsItems
 */
$this->title = $title;
$this->meta_image_url = $meta_image_url;
$home_list_block_id = explode(',', $settings['home_list_block_id']);
?>
<div class="swiper-container swiper-main-container">
    <?php if ($sliders && !empty($sliders)): ?>
        <div class=swiper-wrapper>
            <?php foreach ($sliders as $slider): ?>
                <?= Html::beginTag('a', ['title' => $slider->title, 'href' => $slider->url, 'target' => '_blank', 'class' => 'swiper-slide']); ?>
                <?= Html::img($slider->getImage(), ['alt' => $slider->title, 'style' => ['width' => '100%']]); ?>
                <?= Html::endTag('a'); ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class=swiper-pagination></div>
</div>
<div class=container>
    <div class="row">

        <?php
        if ($settings['home_first_block_id']) {

            if ($fisrt_home_block->children):
                echo $this->import('_category_top', ['models' => $fisrt_home_block->children]);
            endif;

        }

        ?>
    </div>
    <div class=row>
        <div class=col-md-12>
            <a href="<?php echo $settings['home_banner_link_1']; ?>">
                <img src="<?php echo SettingHelper::getSettingImage($settings['home_banner_1']); ?>" alt="home_banner_1"
                     class=img-responsive>
            </a>
        </div>
    </div>
</div>
<div class=container>
    <div class=row>
        <section class=col-md-12>
            <?php
            if ($home_list_block_id && count($home_list_block_id) > 1):
                foreach ($home_list_block_id as $block_id):
                    if ($categories && !empty($categories)): ?>
                        <div class="row main-topic">
                            <?php foreach ($categories as $category):
                                if ($category->id == $block_id) :
                                    ?>
                                    <section class="homepage-row-product">
                                        <div class="homepage-row-product-title text-center">
                                            <h2><?= Html::a($category->title, $category->getUrl(), ['title' => $category->title]) ?></h2>
                                        </div>
                                        <div class="">
                                            <?php if ($category->children) {
                                                echo $this->import('_category_children', ['models' => $category->children]);
                                            } ?>
                                        </div>
                                    </section>
                                <?php
                                endif;
                            endforeach; ?>
                        </div>
                    <?php endif;
                endforeach;
            endif;
            ?>
        </section>
    </div>
    <?php if ($newsItems && !empty($newsItems)): ?>
        <hr>
        <div class="row hottopic-section">
            <?php foreach ($newsItems as $news): ?>
                <div class="col-xs-6 col-sm-4 col-md-4">
                    <div class="hot-box" itemscope itemtype="http://schema.org/NewsArticle">
                        <a href="<?= $news->getUrl() ?>" title="<?= Html::decode($news->title) ?>">
                            <div class="img-wrap">
                                <?= Html::img($news->getImage(), ['alt' => $news->title]) ?>
                            </div>
                        </a>
                        <div class="hot-caption text-justify">
                            <h3 itemprop=name>
                                <?= Html::a($news->title, $news->getUrl()) ?>
                            </h3>
                            <p><?= $news->intro && ($intro = strip_tags($news->intro)) !== null ?
                                StringHelper::truncateWords($intro, 60) : null ?></div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif ?>
</div>
<div class="embed-responsive embed-responsive-video">
    <?php echo $settings['home_master_video']; ?>

    <!-- Replace with more video from youtube here! -->
</div>
<div class=container>
    <div class=row>
        <section class=col-md-12>
            <div class="homepage-row-product-title text-center"><h2></h2></div>
            <div class=col-md-6>
                <div class=homeContent><p>
                    <ul style=margin-left:5px>
                        <li>
                            <span style=font-size:12pt>Đội ngũ nhân viên và công nhân gắn bó lâu dài</span>
                        </li>
                        <li><span style=font-size:12pt>Tư vấn thiết kế miễn phí</span></li>
                        <li><span style=font-size:12pt>Bảo hành và bảo trì sau khi hoàn công</span></li>
                        <li><span style=font-size:12pt>Hoàn tiền có khách nếu có sự cố không đạt yêu cầu</span></li>
                        <li><span style=font-size:12pt>Đội ngũ nhân sự là kiến trúc sư nhiều năm kinh nghiệm</span></li>
                        <li><span style=font-size:12pt>Cam kết thực hiện đúng tiến độ 100%</span></li>
                    </ul>
                </div>
            </div>
            <div class=col-md-6>
                <img src="<?php echo SettingHelper::getSettingImage($settings['home_banner_2']); ?>" alt="home_banner_2"
                     class=img-responsive>
            </div>
        </section>
    </div>
    <?php if ($testimonials && !empty($testimonials)): ?>
        <div class=container>
            <div class=row>
                <section class=col-md-12 style=display:flex;flex-wrap:wrap>
                    <?php foreach ($testimonials as $item): ?>
                        <div class="col-xs-6 col-md-3">
                            <div>
                                <div class="img-wrap square circle">
                                    <?= Html::img($item->getImage(), ['alt' => $item->full_name]) ?>
                                </div>
                                <h3 class="csname text-center"><?= Html::decode($item->full_name) ?></h3>
                                <p><?= $item->intro && !empty($item->intro) ? StringHelper::truncateWords(
                                        $item->intro, 200) : null ?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </section>
            </div>
        </div>
    <?php endif; ?>
    <div class=row>
        <section class=col-md-12>
            <?= NewsWidget::register($this) ?>
        </section>
    </div>
</div>

<section>
    <div class="container">
        <div class="row">
            <?php for ($i = 1; $i <= 3; $i++): ?>
                <div class="col-md-4 col-lg-4 col-xs-4">
                    <button type="button" data-toggle="modal" data-target="#video-<?= $i ?>"
                            data-src="<?php echo $settings['home_video_link_' . $i]; ?>"
                            class="img-wrap anim player">
                        <img src="<?php echo SettingHelper::getSettingImage($settings['home_video_thumb_' . $i]); ?>"
                             alt="">
                    </button>
                    <div class="modal fade video-modal" id="video-<?= $i ?>" tabindex="-1" role="dialog"
                         aria-labelledby="video-<?= $i ?>">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                #Video <?= $i ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endfor; ?>
        </div>
    </div>
</section>

<div class="container">
    <?php if ($partners && !empty($partners)): ?>
        <div class=row>
            <section class=col-md-12>
                <div class=row>
                    <div class="homepage-row-product-title text-center">
                        <h2><a>BÁO CHÍ NÓI GÌ VỀ CHÚNG TÔI</a></h2>
                    </div>
                </div>
                <div class="swiper-container swiper-partner-container hidden-xs">
                    <div class="swiper-wrapper">
                        <?php foreach ($partners as $partner): ?>
                            <div class="swiper-slide">
                                <div class="text-center">
                                    <a rel="nofollow" href="<?= $partner->getUrl() ?>" target=_blank
                                       title="<?= Html::decode($partner->title) ?>">
                                        <?= Html::img($partner->getImage(), ['alt' => $partner->title, 'style' => ['height' => '80px']]) ?>
                                    </a>
                                </div>

                            </div>
                        <?php endforeach; ?>

                    </div>
                    <div class="swiper-partner-container"></div>
                </div>
                <div class="swiper-container swiper-partner-container-mobile visible-xs">
                    <div class="swiper-wrapper">
                        <?php foreach ($partners as $partner): ?>
                            <div class="swiper-slide">
                                <div class="text-center">
                                    <a rel="nofollow" href="<?= $partner->getUrl() ?>" target=_blank
                                       title="<?= Html::decode($partner->title) ?>">
                                        <?= Html::img($partner->getImage(), ['alt' => $partner->title, 'style' => ['width' => '80%']]) ?>
                                    </a>
                                </div>

                            </div>
                        <?php endforeach; ?>

                    </div>
                    <div class="swiper-partner-container"></div>
                </div>
            </section>
        </div>
    <?php endif ?>
</div>